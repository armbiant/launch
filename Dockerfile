FROM node:17-slim

WORKDIR /usr/src/app

ADD . .

RUN npm install --force
RUN npm run build

EXPOSE 8080

ENV HTTPS true

# Read default portal from the local environment
ENV MERGE_PORTAL "$MERGE_PORTAL"
ENV MERGE_API_URI "$MERGE_API_URI"
ENV MERGE_AUTH_URI "$MERGE_AUTH_URI"

ENTRYPOINT npx react-inject-env set -d ./dist && npm run -d start
