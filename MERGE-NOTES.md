Merge Development Notes
=======================

# TLS

We inject enviroment at run time in the launch container instead oi build time. This way 
we support running a single launch container against different portals at different URLS. 
This works by generating a file at `/env.js` that holds the desired environment. This
file is read at run time. 

So to run in dev mode, we need to generate this file and the ./dist directory. The runtime 
will serve from / and ./dist which makes this work. 



```
# preflight. rerun if you update .env* at all.
mkdir ./dist
npx react-inject-env set -d ./dist   # read .env and generate ./dist/env.js

# run the application
sudo -E HTTPS=true npm run start:dev -- --https --host console.mergetb.example.net
```
