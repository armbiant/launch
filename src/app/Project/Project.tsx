import * as React from 'react';
import { useContext } from 'react';
import {
  Alert,
  PageSection,
  TextInput,
  Card,
  CardHeader,
  CardBody,
  CardActions,
  Bullseye,
  Spinner,
  Breadcrumb,
  BreadcrumbItem,
  Title,
  Modal,
  ModalVariant,
  Form,
  FormGroup,
  FormAlert,
  FormSelect,
  FormSelectOption,
  Button,
} from '@patternfly/react-core';
import { TableComposable, Thead, Tbody, Tr, Th, Td } from '@patternfly/react-table';
import { useParams, Link } from 'react-router-dom';
import { GeneralSettingsContext } from '@app/Settings/General/GeneralSettings';
import { useFetch } from 'use-http';
import { FetchSelect } from '@app/lib/FetchSelect';
import { sortable, headerCol } from '@patternfly/react-table';
import { ActionList } from '@app/lib/ActionList';

const Project: React.FunctionComponent = () => {
  // {
  //   "project":  {
  //     "name":  "geoff",
  //     "members":  {
  //       "geoff":  {
  //         "role":  "Creator",
  //         "state":  "Active"
  //       }
  //     },
  //     "experiments":  [
  //       "exp"
  //     ],
  //     "gid":  1029,
  //     "ver":  "2"
  //   }
  // }

  const { pid } = useParams();
  const conf = useContext(GeneralSettingsContext);
  const [reload, setReload] = React.useState(0);

  const options = { credentials: 'include', cachePolicy: 'no-cache' };
  const { loading, error, data } = useFetch(conf.api + '/project/' + pid, options, [reload]);

  const crumbs = (
    <PageSection>
      <Breadcrumb>
        <BreadcrumbItem to="../project">Projects</BreadcrumbItem>
        <BreadcrumbItem>{pid}</BreadcrumbItem>
      </Breadcrumb>
    </PageSection>
  );

  console.log('pdata', data);

  return (
    <React.Fragment>
      {crumbs}
      <PageSection>
        <Card id={pid + '-card-id'}>
          <CardHeader id={pid + 'card-header-id'}>Details</CardHeader>
          <CardBody>
            {error && !data && (
              <Alert variant="danger" title="Error">
                Error loading
              </Alert>
            )}
            {error && data && data.hasOwnProperty('message') && (
              <Alert variant="danger" title="Response Error">
                {data.message}
              </Alert>
            )}
            {loading && (
              <Bullseye>
                <Spinner size="sm" />
              </Bullseye>
            )}
            {data && data.hasOwnProperty('project') && (
              <Form isHorizontal>
                <FormGroup fieldId="name" label="Name">
                  <TextInput value={data.project.name} aria-label="Name" isDisabled />
                </FormGroup>
                <FormGroup fieldId="mode" label="Mode">
                  <TextInput isDisabled aria-label="Access Mode" value={data.project.accessMode} />
                </FormGroup>
                {data.project.experiments.length > 0 && (
                  <FormGroup isInline fieldId="experiments" label="Experiments">
                    {data.project.experiments.sort().map((e, i) => (
                      <Link key={i} to={'/project/' + pid + '/experiment/' + e}>
                        {e}
                      </Link>
                    ))}
                  </FormGroup>
                )}
              </Form>
            )}
          </CardBody>
        </Card>
      </PageSection>
      {data && data.hasOwnProperty('project') && (
        <Members
          reload={() => {
            setReload(reload + 1);
          }}
          project={pid}
          members={data.project.members}
        />
      )}
    </React.Fragment>
  );
};

interface NewMemberProps {
  project: string;
  members: Map<string, Member>;
  isOpen: any;
  onClose: any;
}

const NewMember: React.FunctionComponent<NewMemberProps> = (props) => {
  const conf = useContext(GeneralSettingsContext);
  const [member, setMember] = React.useState({ value: '', valid: 'error' });
  const [memState, setMemState] = React.useState('Active');
  const [role, setRole] = React.useState('Member');

  const submitForm = () => {
    fetch(conf.api + '/project/' + props.project + '/member/' + member.value, {
      method: 'PUT',
      credentials: 'include',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        member: {
          role: role,
          state: memState,
        },
      }),
    })
      .then((response) => {
        props.onClose();
        setMember({ value: '', valid: 'error' });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const onMemberSelect = (val) => {
    setMember({ value: val, valid: val === '' ? 'error' : 'success' });
  };

  function mapUsers(json: Record<string, unknown>): Array<string> {
    // filter out existing users from current members.
    const notExistingMember = (element) => {
      let exists = false;
      Object.keys(props.members).forEach((e) => {
        if (e === element.username) {
          exists = true;
        }
      });
      return !exists;
    };

    const notMembers = json.users.filter(notExistingMember);
    return notMembers.map((u) => u.username);
  }

  return (
    <Modal isOpen={props.isOpen} onClose={props.onClose} variant={ModalVariant.medium} aria-label="Add Project Member">
      <Card>
        <CardHeader>Add Project Member</CardHeader>
        <CardBody>
          <Form isHorizontal>
            {(member.valid !== 'success' || member.valid !== 'success') && (
              <FormAlert>
                <Alert variant="danger" title="All required fields must be filled" aria-live="polite" isInline />
              </FormAlert>
            )}
            <FormGroup fieldId="project" label="Project">
              <TextInput value={props.project} aria-label="Project" isDisabled />
            </FormGroup>
            <FormGroup fieldId="member" label="Member" validated={member.valid} isRequired={true}>
              <FetchSelect label="Member" url={conf.api + '/users'} onSelect={onMemberSelect} mapItems={mapUsers} />
            </FormGroup>
            <FormGroup fieldId="role" label="Role">
              <FormSelect value={role} aria-label="Role" onChange={(v) => setRole(v)}>
                <FormSelectOption key={0} value="Member" label="Member" />
                <FormSelectOption key={1} value="Maintainer" label="Maintainer" />
                <FormSelectOption key={2} value="Creator" label="Creator" />
              </FormSelect>
            </FormGroup>
            <FormGroup fieldId="state" label="State">
              <FormSelect value={memState} aria-label="State" onChange={(v) => setMemState(v)}>
                <FormSelectOption key={0} value="Active" label="Active" />
                <FormSelectOption key={1} value="Pending" label="Pending" />
              </FormSelect>
            </FormGroup>
            <FormGroup fieldId="submit">
              <Button
                variant="primary"
                onClick={submitForm}
                isActive={member.valid !== 'error' && member.valid !== 'error'}
                isAriaDisabled={member.valid === 'error' || member.valid === 'error'}
              >
                Submit
              </Button>
            </FormGroup>
          </Form>
        </CardBody>
      </Card>
    </Modal>
  );
};

interface Member {
  username: string;
  role: string;
  state: string;
}

interface MembersProps {
  project: string;
  members: Map<string, Member>;
  reload: any;
}

const Members: React.FunctionComponent<MembersProps> = ({ project, members, reload }) => {
  const [isModalOpen, setIsModalOpen] = React.useState(false);
  const conf = useContext(GeneralSettingsContext);

  const cols = [
    { title: 'Username', cellTransforms: [headerCol()], transforms: [sortable] },
    { title: 'Role', cellTransforms: [headerCol()], transforms: [sortable] },
    { title: 'State', cellTransforms: [headerCol()], transforms: [sortable] },
  ];
  const rows = Object.keys(members).map((user) => [
    { title: <Link to={'/user/' + user}>{user}</Link>, props: { component: 'th', text: user } },
    members[user].role,
    members[user].state,
  ]);

  const updateMember = (member, role, state) => {
    fetch(conf.api + '/project/' + project + '/member/' + member, {
      method: 'POST',
      credentials: 'include',
      body: JSON.stringify({
        member: {
          role: role,
          state: state,
        },
      }),
    })
      .then((response) => {
        reload();
      })
      .catch((error) => console.log(error));
  };

  const actions = [
    {
      title: 'Remove from project',
      onClick: (event, rowId, rowData) => {
        const member = rowData[0].props.text;
        fetch(conf.api + '/project/' + project + '/member/' + member, {
          method: 'DELETE',
          credentials: 'include',
        })
          .then((response) => {
            reload();
          })
          .catch((error) => console.log(error));
      },
    },
    {
      title: 'Set as maintainer',
      onClick: (event, rowId, rowData) => {
        const member = rowData[0].props.text;
        const state = rowData[2];
        const role = 'Maintainer';
        updateMember(member, role, state);
      },
    },
    {
      title: 'Set as member',
      onClick: (event, rowId, rowData) => {
        const member = rowData[0].props.text;
        const state = rowData[2];
        const role = 'Member';
        updateMember(member, role, state);
      },
    },
    {
      title: 'Set active',
      onClick: (event, rowId, rowData) => {
        const member = rowData[0].props.text;
        const role = rowData[1];
        const state = 'Active';
        updateMember(member, role, state);
      },
    },
    {
      title: 'Set inactive',
      onClick: (event, rowId, rowData) => {
        const member = rowData[0].props.text;
        const role = rowData[1];
        const state = 'Pending';
        updateMember(member, role, state);
      },
    },
  ];

  const toggleModal = () => {
    setIsModalOpen(!isModalOpen);
    reload();
  };

  return (
    <React.Fragment>
      <NewMember project={project} members={members} isOpen={isModalOpen} onClose={toggleModal} />
      <PageSection>
        <Card id="project-members-card">
          <CardHeader id="project-card-header-id">
            <CardActions>
              <Button variant="control" aria-label="Add Member" onClick={() => setIsModalOpen(true)}>
                Add Member
              </Button>
            </CardActions>
            Members
          </CardHeader>
          <CardBody>
            <ActionList kind="Project Members" columns={cols} rows={rows} actions={actions} />
          </CardBody>
        </Card>
      </PageSection>
    </React.Fragment>
  );
};

export { Project };
