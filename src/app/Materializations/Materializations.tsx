import * as React from 'react';
import {
  PageSection,
  Title,
  Breadcrumb,
  BreadcrumbItem,
  Split,
  SplitItem,
  Button,
  Alert,
  AlertGroup,
  AlertActionCloseButton,
} from '@patternfly/react-core';
import { Link } from 'react-router-dom';
import { ActionList } from '@app/lib/ActionList';
import { sortable, headerCol } from '@patternfly/react-table';
import { GeneralSettingsContext } from '@app/Settings/General/GeneralSettings';
import { UserContext } from '@app/User/User';
import { ColorStringFromStatus, LocalizeDate } from '@app/lib/TaskStatusUtils';

const Materializations: React.FunctionComponent = () => {
  var userview_last = (localStorage.getItem('userview') ?? true) == true;

  const conf = React.useContext(GeneralSettingsContext);
  const [reload, setReload] = React.useState(false);
  const [alerts, setAlerts] = React.useState([]);
  const idTraits = React.useContext(UserContext);

  const [viewLabel, setViewLabel] = React.useState('View ' + (userview_last ? 'All' : 'Own'));
  const [userView, setUserView] = React.useState(userview_last);

  const columns = [
    { title: 'Materialization', cellTransforms: [headerCol()], transforms: [sortable] },
    { title: 'Metal Nodes' },
    { title: 'Virtual Nodes' },
    { title: 'Links' },
    { title: 'Created' },
    { title: 'Last Updated' },
    { title: 'Status' },
  ];

  const mapper = (json) =>
    json.materializations.map((x, i) => [
      {
        title: (
          <Link key={i} to={'/materializations/' + x.pid + '/' + x.eid + '/' + x.rid}>
            {x.rid}.{x.eid}.{x.pid}
          </Link>
        ),
        props: {
          component: 'th',
          text: x.rid + '.' + x.eid + '.' + x.pid,
        },
      },
      x.metal.length,
      x.vms.length,
      x.links.length,
      LocalizeDate(json.statuses[i].FirstUpdated),
      LocalizeDate(json.statuses[i].LastUpdated),
      ColorStringFromStatus(json.statuses[i].HighestStatus, json.statuses[i].HighestStatus),
    ]);

  const actions = [
    {
      title: 'Dematerialize',
      onClick: (event, rowId, rowData) => {
        const ids = rowData[0].props.text.split('.');

        fetch(conf.api + '/materialize/materialize/' + ids[2] + '/' + ids[1] + '/' + ids[0], {
          method: 'DELETE',
          credentials: 'include',
        })
          .then((response) => {
            if (response.ok) {
              return response.json();
            } else {
              return response.text().then((text) => {
                throw new Error(text);
              });
            }
          })
          .then((json) => {
            addAlert('Dematerialization ' + ids[0] + '.' + ids[1] + '.' + ids[2] + ' started.', '', 'success');
            setReload(!reload);
          })
          .catch((error) => {
            console.log('got error:', error);
            addAlert('Dematerialize Error', error.message, 'danger');
          });
      },
    },
  ];

  const toggleView = () => {
    setViewLabel('View ' + (userView === false ? 'All' : 'Own'));
    localStorage.setItem('userview', userView ? '0' : '1');
    setUserView(!userView);
    setReload(!reload);
  };

  const crumbs = (
    <PageSection>
      <Breadcrumb>
        <BreadcrumbItem>User</BreadcrumbItem>
        <BreadcrumbItem to={'/users/' + idTraits.username}>{idTraits.username}</BreadcrumbItem>
        <BreadcrumbItem>Materializations</BreadcrumbItem>
      </Breadcrumb>
    </PageSection>
  );

  const header = (
    <PageSection>
      <Split>
        <SplitItem>
          <Title headingLevel="h1" size="lg">
            Materializations
          </Title>
        </SplitItem>
        <SplitItem isFilled />
        <SplitItem>
          <Button variant="control" aria-label={viewLabel} onClick={toggleView}>
            {viewLabel}
          </Button>
        </SplitItem>
      </Split>
    </PageSection>
  );

  const addAlert = (t, m, v) => {
    setAlerts((prev) => [...prev, { title: t, message: m, variant: v, key: new Date().getTime() }]);
  };

  const removeAlert = (key) => {
    setAlerts([...alerts.filter((el) => el.key !== key)]);
  };

  const notifications = (
    <AlertGroup isToast>
      {alerts.map((a, i) => (
        <Alert
          isExpandable={a.message !== ''}
          variant={a.variant}
          title={a.title}
          key={a.key}
          timeout={8000}
          actionClose={
            <AlertActionCloseButton
              title={a.title}
              variantLabel={`${a.variant} alert`}
              onClose={() => removeAlert(a.key)}
            />
          }
        >
          {a.message}
        </Alert>
      ))}
    </AlertGroup>
  );

  let url = conf.api + '/materialize/materializations';
  if (userView === false) {
    url += '?filter=ByAll';
  }

  return (
    <React.Fragment>
      {alerts.length !== 0 && notifications}
      {crumbs}
      {header}
      <PageSection>
        <ActionList
          kind="Materializations"
          columns={columns}
          url={url}
          actions={actions}
          mapper={mapper}
          reload={reload}
        />
      </PageSection>
    </React.Fragment>
  );
};

export { Materializations };
