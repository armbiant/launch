import * as React from 'react';
import { PageSection, Title, Breadcrumb, BreadcrumbItem } from '@patternfly/react-core';
import { Link } from 'react-router-dom';
import { ActionList } from '@app/lib/ActionList';
import { sortable, headerCol } from '@patternfly/react-table';
import { GeneralSettingsContext } from '@app/Settings/General/GeneralSettings';

const Users: React.FunctionComponent = () => {
  const conf = React.useContext(GeneralSettingsContext);

  let columns = [
    { title: 'Username', cellTransforms: [headerCol()], transforms: [sortable] },
    { title: 'Name', cellTransforms: [headerCol()], transforms: [sortable] },
    // { title: 'Email', transforms: [sortable] },
    { title: 'State', transforms: [sortable] },
    { title: 'Access', transforms: [sortable] },
    { title: 'Projects' },
    { title: 'Experiments' },
    { title: 'UID' },
    { title: 'GID' },
  ];

  const actions = {};
  // TODO: make an admin view of users that allows actions like these.
  // let actions = [
  //     {
  //         title: 'Activate',
  //         onClick: (event, rowId, rowData, extra) => {
  //             console.log('Activating user: ', rowData.name.title)
  //             fetch("https://admin.mergetb.example.net/user/activate/"+rowData.name.title, {
  //                 method: 'POST',
  //             }).then(resp => {
  //                 console.log('User activation response')
  //                 console.log(resp)
  //             })
  //         }
  //     },
  //     {
  //         title: 'Edit',
  //         onClick: (event, rowId, rowData, extra) => console.log('Editing user: ', rowData.name)
  //     },
  //     {
  //         isSeparator: true
  //     },
  //     {
  //         title: 'Freeze',
  //         onClick: (event, rowId, rowData, extra) => {
  //             console.log('Freezing user: ', rowData.name.title)
  //             fetch("https://admin.mergetb.example.net/user/freeze/"+rowData.name.title, {
  //                 method: 'POST',
  //             }).then(resp => {
  //                 console.log('User freeze response')
  //                 console.log(resp)
  //             })
  //         }
  //     },
  //     {
  //         title: 'Delete',
  //         onClick: (event, rowId, rowData, extra) => {
  //             console.log('Deleting user: ', rowData.name.title)
  //             fetch("https://admin.mergetb.example.net/user/"+rowData.name.title, {
  //                 method: 'DELETE',
  //             }).then(resp => {
  //                 console.log('User delete response')
  //                 console.log(resp)
  //             })
  //         }
  //     }
  // ];

  const mapper = (json) =>
    json.users.map((x) => [
      {
        title: <Link to={'/user/' + x.username}>{x.username}</Link>,
        props: {
          component: 'th',
          text: x.username,
        },
      },
      x.name,
      x.state,
      x.accessMode,
      Object.keys(x.projects).length,
      x.experiments.length,
      x.uid,
      x.gid,
    ]);

  const crumbs = (
    <PageSection>
      <Breadcrumb>
        <BreadcrumbItem>Users</BreadcrumbItem>
      </Breadcrumb>
    </PageSection>
  );

  const header = (
    <PageSection>
      <Title headingLevel="h1" size="lg">
        Users
      </Title>
    </PageSection>
  );

  return (
    <React.Fragment>
      {crumbs}
      {header}
      <PageSection>
        <ActionList kind="User" columns={columns} url={conf.api + '/users'} actions={actions} mapper={mapper} />
      </PageSection>
    </React.Fragment>
  );
};

export { Users };
