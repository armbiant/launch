import * as React from 'react';
import { useContext } from 'react';
import '@patternfly/react-core/dist/styles/base.css';
import { BrowserRouter as Router } from 'react-router-dom';
import { AppLayout } from '@app/AppLayout/AppLayout';
import { AppRoutes } from '@app/routes';
// import { MergeLogin } from '@app/Login/Login';
import { UserContext } from '@app/User/User';
import { GeneralSettingsContext, Gconf } from '@app/Settings/General/GeneralSettings';
import { Provider as UseHttpProvider } from 'use-http';
import { AppLoading } from '@app/AppLoading/AppLoading';
import { env } from '@app/env.js';
import '@app/app.css';

const App: React.FunctionComponent = () => {
  // defaults from env, then load remote if available.
  const portal = localStorage.getItem('portal') ?? env.REACT_APP_MERGE_PORTAL;

  console.log('using MERGE_PORTAL', portal);

  // This function gets passed down to components in the tree so they
  // can update the global configuraioni/context from there.
  const setPortal = (value, conf) => {
    for (const name in conf.testbeds) {
      if (name === value) {
        if (value !== conf.portal) {
          // TODO find a clearer way to do this.
          const href =
            conf.testbeds[value].auth +
            '/.ory/kratos/self-service/browser/flows/logout?return_to=' +
            window.location.href;
          localStorage.setItem('portal', value);

          setTbconf({
            testbeds: conf.testbeds,
            portal: value,
            api: conf.testbeds[value].api,
            auth: conf.testbeds[value].auth,
            update: setPortal,
          });

          window.location.href = href;
        }
      }
    }
  };

  const [tbconf, setTbconf] = React.useState({
    // known testbeds
    testbeds: {},

    // chosen portal and portal urls at toplevel for easy access
    portal: portal,
    api: env.REACT_APP_MERGE_API_URI,
    auth: env.REACT_APP_MERGE_AUTH_URI,

    // update function so nested elements can update the configuration.
    update: setPortal,
  });

  React.useEffect(() => {
    if (env.REACT_APP_KNOWN_TESTBEDS_URI !== '') {
      fetch(env.REACT_APP_KNOWN_TESTBEDS_URI, { headers: { 'Content-Type': 'application/json' } })
        .then((res) => {
          if (res.ok) {
            return res.json();
          }
          throw new Error('loading remote config');
        })
        .then((json) => {
          console.log('loaded remote config', json);
          // if there is a local choice og testbed to use, use it else use env.
          setTbconf({
            testbeds: json.testbeds,
            api: json.testbeds[portal].api,
            auth: json.testbeds[portal].auth,
            portal: portal,
            update: setPortal,
          });
        })
        .catch((error) => {
          console.log('error loading remote configuration', error);
        });
    }
  }, []);

  // This does not work for some reason. TODD: figure out why.
  const useHttpOpts = {
    interceptors: {
      request: ({ options }) => {
        options.headers = {
          credentials: 'include',
        };
        return options;
      },
      response: ({ response }) => {
        return response;
      },
    },
  };

  return (
    <GeneralSettingsContext.Provider value={tbconf}>
      <UseHttpProvider options={useHttpOpts}>
        <Main />
      </UseHttpProvider>
    </GeneralSettingsContext.Provider>
  );
};

const Main: React.FunctionComponent = () => {
  const [content, setContent] = React.useState(<AppLoading />);
  const conf = useContext(GeneralSettingsContext);

  React.useEffect(() => {
    console.log("main: auth'ing against", conf.auth);

    fetch(conf.auth + '/.ory/kratos/sessions/whoami', { credentials: 'include' })
      .then((resp) => {
        if (resp.status == 200) {
          return resp.json();
        } else if (resp.status == 401) {
          const href = conf.auth + '/.login/web/auth/login?return_to=' + window.location.href;
          window.location.href = href;
        } else {
          setContent(
            <React.Fragment>
              <h1> An unknown error occured </h1>
              <p>
                {' '}
                {resp.status}: {resp.statusText}{' '}
              </p>
            </React.Fragment>
          );
        }
      })
      .then((body) => {
        setContent(
          <Router>
            <UserContext.Provider value={body.identity.traits}>
              <AppLayout>
                <AppRoutes />
              </AppLayout>
            </UserContext.Provider>
          </Router>
        );
      });
  }, [conf.auth]);

  return content;
};

export default App;
