import * as React from 'react';
import { useContext } from 'react';
import { Link, useParams } from 'react-router-dom';
import {
  PageSection,
  Title,
  Card,
  CardHeader,
  CardTitle,
  CardBody,
  Alert,
  Bullseye,
  Spinner,
  Form,
  FormGroup,
  TextArea,
  TextInput,
  Breadcrumb,
  BreadcrumbItem,
  Popover,
  Button,
  Tooltip,
  Text,
} from '@patternfly/react-core';
import { GeneralSettingsContext } from '@app/Settings/General/GeneralSettings';
import { useFetch } from 'use-http';
import { ActionList } from '@app/lib/ActionList';
import { sortable, headerCol } from '@patternfly/react-table';

// exp json example
// {
//   "experiment":  {
//     "name":  "aaa",
//     "project":  "aaa",
//     "models":  {
//       "a4227a99e12fec40038fe2e5b8a3970bc4850a23":  {
//         "compiled":  true,
//         "msg":  "Succeeded",
//         "compileTime":  "2021-08-16T20:49:06.849481137Z"
//       }
//     },
//     "repository":  "https://git.dorvan.mergetb.net/aaa/aaa",
//     "creator":  "glawler",
//     "ver":  "2"
//   }
// }

const Experiment: React.FunctionComponent = () => {
  const { pid, eid } = useParams();
  const conf = useContext(GeneralSettingsContext);

  const options = { credentials: 'include' };
  const { loading, error, data } = useFetch(conf.api + '/project/' + pid + '/experiment/' + eid, options, []);

  const crumbs = (
    <PageSection>
      <Breadcrumb>
        <BreadcrumbItem to="/project">Projects</BreadcrumbItem>
        <BreadcrumbItem to={'/project/' + pid}>{pid}</BreadcrumbItem>
        <BreadcrumbItem to="/experiment">Experiments</BreadcrumbItem>
        <BreadcrumbItem>{eid}</BreadcrumbItem>
      </Breadcrumb>
    </PageSection>
  );

  const details = (
    <Card id="deetsCard">
      <CardHeader>
        <CardTitle id="deetsCardTitle">Details</CardTitle>
      </CardHeader>
      <CardBody>
        {error && !data && (
          <Alert variant="danger" title="Error">
            Error loading
          </Alert>
        )}
        {error && data && data.hasOwnProperty('message') && (
          <Alert variant="danger" title="Response Error">
            {data.message}
          </Alert>
        )}
        {loading && (
          <Bullseye>
            <Spinner size="sm" />
          </Bullseye>
        )}
        {data && data.hasOwnProperty('experiment') && (
          <Form isHorizontal>
            <FormGroup fieldId="name" label="Name">
              <TextInput value={data.experiment.name} aria-label="Name" isDisabled />
            </FormGroup>
            <FormGroup fieldId="project" label="Project">
              <TextInput aria-label="Project" value={data.experiment.project} isDisabled />
            </FormGroup>
            <FormGroup fieldId="repository" label="Repository">
              <TextInput value={data.experiment.repository} aria-label="Repository" isDisabled />
            </FormGroup>
            <FormGroup fieldId="creator" label="Creator">
              <TextInput value={data.experiment.creator} aria-label="Creator" isDisabled />
            </FormGroup>
            {data.experiment.maintainers.length !== 0 && (
              <FormGroup fieldId="maintainers" label="Maintainers">
                <TextInput aria-label="Maintainers" value={data.experiment.maintainers} isDisabled />
              </FormGroup>
            )}
            <FormGroup fieldId="desc" label="Description">
              <TextArea value={data.experiment.description} aria-label="Description" isDisabled />
            </FormGroup>
          </Form>
        )}
      </CardBody>
    </Card>
  );

  const revisions = (
    <React.Fragment>
      {error && !data && (
        <Alert variant="danger" title="Error">
          Error loading
        </Alert>
      )}
      {error && data && data.hasOwnProperty('message') && (
        <Alert variant="danger" title="Response Error">
          {data.message}
        </Alert>
      )}
      {loading && (
        <Bullseye>
          <Spinner size="sm" />
        </Bullseye>
      )}
      {data && data.hasOwnProperty('experiment') && (
        <Revisions pid={data.experiment.project} eid={data.experiment.name} models={data.experiment.models} />
      )}
    </React.Fragment>
  );

  return (
    <React.Fragment>
      {crumbs}
      <PageSection>
        <Title headingLevel="h1" size="3xl">
          Experiment: {pid}.{eid}
        </Title>
      </PageSection>
      <PageSection>{details}</PageSection>
      <PageSection>{revisions}</PageSection>
    </React.Fragment>
  );
};

type ExpRevProps = {
  pid: string;
  eid: string;
  models: any;
};

const Revisions: React.FunctionComponent<ExpRevProps> = ({ pid, eid, models }) => {
  const cols = [
    { title: 'Revision', cellTransforms: [headerCol()], transforms: [sortable] },
    { title: 'Realizations' },
    { title: 'Compilation' },
    { title: 'Timestamp', cellTransforms: [headerCol()], transforms: [sortable] },
  ];

  const keys = Object.keys(models).sort((a, b) => {
    const d1 = Date.parse(models[a].compileTime);
    const d2 = Date.parse(models[b].compileTime);
    return d2 - d1;
  });

  const rows = keys.map((rev, i) => [
    models[rev].compiled ? (
      {
        title: (
          <React.Fragment key={i}>
            <Tooltip content={rev}>
              <Link to={'/model/' + pid + '/' + eid + '/' + rev}>{rev.substring(0, 8)}</Link>
            </Tooltip>
          </React.Fragment>
        ),
        props: {
          component: 'th',
          text: rev,
        },
      }
    ) : (
      <React.Fragment key={i}>
        <Tooltip content={rev}>
          <Text>{rev.substring(0, 8)}</Text>
        </Tooltip>
      </React.Fragment>
    ),
    <React.Fragment key={'reals-' + i}>
      {models[rev].realizations.map((rlz, ri) => (
        <React.Fragment key={ri}>
          <Link to={'/realizations/' + pid + '/' + eid + '/' + rlz} key={ri}>
            {rlz}
          </Link>
          <p />
        </React.Fragment>
      ))}
    </React.Fragment>,
    models[rev].compiled ? (
      models[rev].msg === 'Succeeded' ? (
        'Succeeded'
      ) : (
        'Failed'
      )
    ) : (
      <React.Fragment>
        <Popover
          aria-label="Compilation Error"
          headerContent={<div>Compilation Error Message</div>}
          bodyContent={models[rev].msg}
        >
          <Button variant="danger">Compile Error</Button>
        </Popover>
      </React.Fragment>
    ),
    new Date(models[rev].compileTime).toLocaleString(),
  ]);

  return (
    <React.Fragment>
      <Card>
        <CardHeader>
          <CardTitle id="expModelsTitle">Revisions</CardTitle>
        </CardHeader>
        <CardBody>
          <ActionList kind="revisions" columns={cols} rows={rows} />
        </CardBody>
      </Card>
    </React.Fragment>
  );
};

export { Experiment };
