import * as React from 'react';
import { useParams } from 'react-router-dom';
import { GeneralSettingsContext } from '@app/Settings/General/GeneralSettings';
import { useFetch } from 'use-http';
import {
  PageSection,
  Breadcrumb,
  BreadcrumbItem,
  Card,
  CardHeader,
  CardBody,
  Spinner,
  Bullseye,
  Alert,
} from '@patternfly/react-core';

type UserContextType = {
  username: string;
  email: string;
};

const UserContext = React.createContext<UserContextType>();

const User: React.FunctionComponent = () => {
  const { uid } = useParams();
  const conf = React.useContext(GeneralSettingsContext);

  const options = { credentials: 'include' };
  const { loading, error, data } = useFetch(conf.api + '/user/' + uid, options, []);

  const crumbs = (
    <PageSection>
      <Breadcrumb>
        <BreadcrumbItem>Users</BreadcrumbItem>
        <BreadcrumbItem>{uid}</BreadcrumbItem>
      </Breadcrumb>
    </PageSection>
  );

  return (
    <React.Fragment>
      {crumbs}
      <PageSection>
        <Card id={uid + '-card-id'}>
          <CardHeader id={uid + '-cardheader-id'}>Details</CardHeader>
          <CardBody>
            {error && !data && (
              <Alert variant="danger" title="Error">
                Error loading
              </Alert>
            )}
            {error && data && data.hasOwnProperty('message') && (
              <Alert variant="danger" title="Response Error">
                <pre>{JSON.stringify(data, null, 2)}</pre>
              </Alert>
            )}
            {loading && (
              <Bullseye>
                <Spinner size="sm" />
              </Bullseye>
            )}
            {data && data.hasOwnProperty('user') && <pre>{JSON.stringify(data.user, null, 2)}</pre>}
          </CardBody>
        </Card>
      </PageSection>
    </React.Fragment>
  );
};

export { UserContext, User };
