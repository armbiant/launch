import * as React from 'react';
import { PageSection, Title } from '@patternfly/react-core';
import { ActionList } from '@app/lib/ActionList';
import { sortable, headerCol } from '@patternfly/react-table';

const Identity: React.FunctionComponent = () => {
  let columns = [
    { title: 'Name', cellTransforms: [headerCol()], transforms: [sortable] },
    { title: 'Email', transforms: [sortable] },
  ];

  let actions = [
    {
      title: 'Confirm',
      onClick: (event, rowId, rowData, extra) => console.log('Confirming identity: ', rowData.name),
    },
    {
      title: 'Send Verification Email',
      onClick: (event, rowId, rowData, extra) => console.log('Sending verification email: ', rowData.email),
    },
    {
      title: 'Edit',
      onClick: (event, rowId, rowData, extra) => console.log('Editing identity: ', rowData.name),
    },
    {
      isSeparator: true,
    },
    {
      title: 'Delete',
      onClick: (event, rowId, rowData, extra) => console.log('Deleting identity: ', rowData.name),
    },
  ];

  let mapper = (json) => json.identities.map((x) => [x.name, x.email]);

  return (
    <React.Fragment>
      <PageSection>
        <Title headingLevel="h1" size="lg">
          Identity Mangement
        </Title>
      </PageSection>
      <PageSection>
        <ActionList
          kind="Identity"
          columns={columns}
          url={conf.api + '/identities'}
          actions={actions}
          mapper={mapper}
        />
      </PageSection>
    </React.Fragment>
  );
};

export { Identity };
