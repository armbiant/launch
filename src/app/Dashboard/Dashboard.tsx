import * as React from 'react';
import { Redirect } from 'react-router-dom';

const Dashboard: React.FunctionComponent = () => {
  return <Redirect to="/news" />;
};

export { Dashboard };
