import React from 'react';
import { Table, TableHeader, TableBody, treeRow, IRow, OnTreeRowCollapse } from '@patternfly/react-table';
import RedoIcon from '@patternfly/react-icons/dist/esm/icons/redo-icon';
import CompressArrowsAltIcon from '@patternfly/react-icons/dist/esm/icons/compress-arrows-alt-icon';
import ExpandArrowsAltIcon from '@patternfly/react-icons/dist/esm/icons/expand-arrows-alt-icon';
import {
  GetIconFromStatus,
  LocalizeDate,
  ColorStringFromStatus,
  ColorStringFromLogLevel,
} from '@app/lib/TaskStatusUtils';
import { GeneralSettingsContext } from '@app/Settings/General/GeneralSettings';
import { createFalse } from 'typescript';
import { Truncate } from '@patternfly/react-core';

interface TaskStatusTreeNode {
  id: string;
  name: Object;
  desc: Object;
  type: Object;
  status: Object;
  version: Object;
  when: Object;
  icon: Object;
  num_all_children: number;
  empty?: Object;
  children?: TaskStatusTreeNode[];
}

export const TaskForestParser: TaskStatusTreeNode[] = (tf: any) => {
  if ('tf' in tf) {
    tf = tf.tf;
  }

  var fullname = '';
  if (tf.ParentPath && tf.ParentPath != '') {
    fullname += tf.ParentPath + '|';
  }
  fullname += tf.Goal.Name;

  var children: TaskStatusTreeNode[] = [];

  if (tf.Goal.Type == 'Supergoal') {
    children = tf.Subgoals.map((child) => TaskForestParser(child));
  } else if (tf.Goal.Type == 'Supertask') {
    children = tf.Subtasks.map((child) => TaskTreeParser(child, fullname));
  }

  children = children.reduce((acc, child) => acc.concat(child), []);

  var status = tf.HighestStatus;

  return [
    {
      id: fullname,
      name: tf.Goal.Name,
      desc: tf.Goal.Desc,
      type: tf.Goal.Type,
      status: ColorStringFromStatus(status, status, fullname, 'status'),
      version: tf.Goal.SelfVersion,
      num_all_children: tf.NumChildTasks,
      when: LocalizeDate(tf.LastUpdated),
      icon: GetIconFromStatus(tf.HighestStatus),
      children: children,
    },
  ];
};

export const TaskTreeParser: TaskStatusTreeNode[] = (tt: any, pp: string) => {
  var fullname = '';
  if (pp != '') {
    fullname += pp + '|';
  }
  fullname += tt.Task.TaskKey + '/' + tt.Task.Manager + '/' + tt.Task.Name;

  var children: TaskStatusTreeNode[] = [];

  children = tt.Subtasks.map((child) => TaskTreeParser(child, fullname));
  children = children.reduce((acc, child) => acc.concat(child), []);

  var desc = '/' + tt.Task.Manager + '/' + tt.Task.Name;
  if (tt.Task.Manager == '' || tt.Task.Name == '') {
    desc = '';
  }

  var status = tt.Task.CurrentStatus;

  var level = '';
  if (tt.Task.Messages.length > 0) {
    level = tt.Task.Messages[0].Level;
  }

  var messages = tt.Task.Messages.map((m, index) =>
    ColorStringFromLogLevel(m.Message, m.Level, status, fullname, index)
  );

  return [
    {
      id: fullname,
      name: tt.Task.TaskKey,
      desc: <div>{messages}</div>,
      type: 'Task',
      status: ColorStringFromStatus(status, status, fullname, 'status'),
      icon: GetIconFromStatus(status),
      version: tt.Task.TaskVersion,
      num_all_children: tt.NumChildTasks,
      when: LocalizeDate(tt.Task.When),
      children: children,
    },
  ];
};

interface TaskStatusTableProps {
  kind: string;
  url?: string;
  reload: number;
  ongoingfrequency: number;
  completedfrequency: number;
  scalingfrequency: number;
  variant?: string;
  borders?: string;
  data?: any;
  actions?: any;
  getter?: any;
}

export const TaskStatusTable: React.FunctionComponent<TaskStatusTableProps> = (props) => {
  const dataRef = React.useRef<TaskStatusTreeNode[]>([]);
  const [data, setData] = React.useState<TaskStatusTreeNode[]>([]);
  const [frequency, setFrequency] = React.useState<number>(props.ongoingfrequency);
  const [expandedNodeNames, setExpandedNodeNames] = React.useState<string[]>([]);
  const [sortBy, setSortBy] = React.useState({});
  const [idSearch, setIdSearch] = React.useState('');
  const [alert, setAlert] = React.useState('');
  const [atitle, setATitle] = React.useState('');
  const conf = React.useContext(GeneralSettingsContext);
  const { reload, getter } = props;

  const columnNames = {
    empty: 'Icon',
    name: 'Name',
    desc: 'Description',
    type: 'Type',
    status: 'Status',
    version: 'Version',
    when: 'Last Updated',
  };

  React.useEffect(() => {
    updateData();
  }, [reload, props.data]);

  React.useEffect(() => {
    // console.log(frequency)

    if (props.url && frequency > 0) {
      const interval = setInterval(() => {
        updateData();
      }, frequency);

      return () => clearInterval(interval);
    }
  }, [frequency]);

  const updateData = () => {
    var firstTime = false;
    if (dataRef.current.length == 0) {
      firstTime = true;

      setFrequency(props.ongoingfrequency);
    } else if (dataRef.current[0].status == 'Success' || dataRef.current[0].status.props.children == 'Success') {
      var freq = props.completedfrequency * dataRef.current[0].num_all_children * props.scalingfrequency;
      if (freq < props.completedfrequency) {
        freq = props.completedfrequency;
      }

      setFrequency(freq);
    } else {
      var freq = props.ongoingfrequency * dataRef.current[0].num_all_children * props.scalingfrequency;
      if (freq < props.ongoingfrequency) {
        freq = props.ongoingfrequency;
      }

      setFrequency(freq);
    }

    // console.log("getting data")

    if (props.url) {
      fetch(props.url, { credentials: 'include' })
        .then((response) => {
          if (response.status == 401) {
            let err = new Error();
            err.name = 'autherror';
            throw err;
          }
          if (!response.ok) {
            console.log('response not ok: ', response);
            throw response;
          }
          return response.json();
        })
        .then((json) => {
          dataRef.current = TaskForestParser(props.getter(json));
          setData([...dataRef.current]);
          if (firstTime) {
            setDefaultExpandedNodeNames();
          }
        })
        .catch((error) => {
          if (error.name === 'autherror') {
            const href = conf.auth + '/.login/web/auth/login?return_to=' + window.location.href;
            window.location.href = href;
          } else if (typeof error.json === 'function') {
            error.json().then((json) => {
              setATitle('Merge API Error');
              json.url = props.url;
              setAlert(json);
            });
          } else {
            console.log('task status table error', error);
            setATitle('Network/Fetch Error');
            setAlert(error);
          }
        });
    } else if (props.data) {
      // Use the rows provided instead of fetching them.
      dataRef.current = TaskForestParser(props.data);
      setData([...dataRef.current]);
      if (firstTime) {
        setDefaultExpandedNodeNames();
      }
    }
  };

  const setDefaultExpandedNodeNames = () => {
    // console.log("resetting expanded node names")

    var temp: string[] = [dataRef.current[0].id];

    const find_expanded = (node: TaskStatusTreeNode) => {
      if (node.status != 'Success' && node.status.props.children != 'Success' && node.type == 'Supergoal') {
        temp.push(node.id);
      }

      if (node.children) {
        node.children.forEach((child) => find_expanded(child));
      }
    };

    dataRef.current.forEach((node) => find_expanded(node));
    setExpandedNodeNames(temp);
  };

  const setAllExpandedNodeNames = () => {
    // console.log("resetting expanded node names")

    var temp: string[] = [dataRef.current[0].id];

    const find_expanded = (node: TaskStatusTreeNode) => {
      temp.push(node.id);

      if (node.children) {
        node.children.forEach((child) => find_expanded(child));
      }
    };

    dataRef.current.forEach((node) => find_expanded(node));
    setExpandedNodeNames(temp);
  };

  const getDescendants = (node: TaskStatusTreeNode): TaskStatusTreeNode[] =>
    [node].concat(...(node.children ? node.children.map(getDescendants) : []));

  // We index the tree nodes in the order of the table rows, for looking up by rowIndex
  const flattenedNodes: TaskStatusTreeNode[] = [];

  /**
      Recursive function which flattens the data into an array of flattened IRow objects
      params:
        - nodes - array of a single level of tree nodes
        - level - number representing how deeply nested the current row is
        - posinset - position of the row relative to this row's siblings
        - isHidden - defaults to false, true if this row's parent is expanded
        - currentRowIndex - position of the row relative to the entire table
    */
  const buildRows = (
    [node, ...remainingNodes]: TaskStatusTreeNode[],
    level = 1,
    posinset = 1,
    rowIndex = 0,
    isHidden = false
  ): IRow[] => {
    if (!node) {
      return [];
    }
    const isExpanded = expandedNodeNames.includes(node.id);
    flattenedNodes.push(node);

    const childRows =
      node.children && node.children.length
        ? buildRows(node.children, level + 1, 1, rowIndex + 1, !isExpanded || isHidden)
        : [];

    return [
      {
        cells: [
          node.name,
          node.desc,
          node.type,
          node.status,
          //node.version,
          node.when,
        ],
        props: {
          isExpanded,
          isHidden,
          'aria-level': level,
          'aria-posinset': posinset,
          'aria-setsize': node.children ? node.children.length : 0,
          icon: node.icon,
        },
      },
      ...childRows,
      ...buildRows(remainingNodes, level, posinset + 1, rowIndex + 1 + childRows.length, isHidden),
    ];
  };

  const onCollapse: OnTreeRowCollapse = (_event, rowIndex) => {
    const node = flattenedNodes[rowIndex];
    const isExpanded = expandedNodeNames.includes(node.id);
    setExpandedNodeNames((prevExpanded) => {
      const otherExpandedNodeNames = prevExpanded.filter((name) => name !== node.id);
      return isExpanded ? otherExpandedNodeNames : [...otherExpandedNodeNames, node.id];
    });
  };

  const variant = props.variant ? props.variant : 'compact';
  const borders = props.borders ? props.borders : 'compactBorderless';

  return (
    <div style={{ margin: '10px' }}>
      <button onClick={updateData}>
        <RedoIcon>Refresh</RedoIcon>
      </button>
      <button onClick={setDefaultExpandedNodeNames}>
        <CompressArrowsAltIcon>Compress</CompressArrowsAltIcon>
      </button>
      <button onClick={setAllExpandedNodeNames}>
        <ExpandArrowsAltIcon>Expand</ExpandArrowsAltIcon>
      </button>
      <Table
        isTreeTable
        aria-label="Tree table"
        variant={variant}
        borders={borders}
        cells={[
          {
            title: columnNames.name,
            cellTransforms: [treeRow(onCollapse)],
          },
          columnNames.desc,
          columnNames.type,
          columnNames.status,
          //columnNames.version,
          columnNames.when,
        ]}
        rows={buildRows(data)}
      >
        <TableHeader />
        <TableBody />
      </Table>
    </div>
  );
};
