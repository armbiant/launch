import * as React from 'react';
import { useContext } from 'react';
import {
  PageSection,
  Title,
  Modal,
  ModalVariant,
  Form,
  FormGroup,
  FormAlert,
  FormHelperText,
  Alert,
  TextInput,
  TextArea,
  Split,
  SplitItem,
  Button,
  Breadcrumb,
  BreadcrumbItem,
} from '@patternfly/react-core';
import { ActionList } from '@app/lib/ActionList';
import { FetchSelect } from '@app/lib/FetchSelect';
import { sortable, headerCol } from '@patternfly/react-table';
import { Link } from 'react-router-dom';
import { UserContext } from '@app/User/User';
import { GeneralSettingsContext } from '@app/Settings/General/GeneralSettings';

const Experiments: React.FunctionComponent = () => {
  var userview_last = (localStorage.getItem('userview') ?? true) == true;

  const conf = useContext(GeneralSettingsContext);
  const [reload, setReload] = React.useState(false);
  const [isModalOpen, setIsModalOpen] = React.useState(false);

  const [viewLabel, setViewLabel] = React.useState('View ' + (userview_last ? 'All' : 'Own'));
  const [userView, setUserView] = React.useState(userview_last);

  let columns = [
    { title: 'Name', cellTransforms: [headerCol()], transforms: [sortable] },
    { title: 'Project', cellTransforms: [headerCol()], transforms: [sortable] },
    { title: 'Access Mode', transforms: [sortable] },
    { title: 'Creator', transforms: [sortable] },
  ];

  let actions = [
    // {
    //     title: 'Edit',
    //     onClick: (event, rowId, rowData, extra) => console.log('Editing project: ', rowData.name)
    // },
    // {
    //     isSeparator: true
    // },
    {
      title: 'Delete',
      onClick: (event, rowId, rowData) => {
        const pid = rowData[1].props.text;
        const eid = rowData[0].props.text;

        fetch(conf.api + '/project/' + pid + '/experiment/' + eid, {
          method: 'DELETE',
          credentials: 'include',
        })
          .then((response) => {
            setReload(!reload);
          })
          .catch((error) => {
            // TODO take evasive action
          });
      },
    },
  ];

  const toggleView = () => {
    setViewLabel('View ' + (userView === false ? 'All' : 'Own'));
    localStorage.setItem('userview', userView ? '0' : '1');
    setUserView(!userView);
    setReload(!reload);
  };

  let mapper = (json) => {
    return json.experiments.map((x) => [
      {
        title: <Link to={'/project/' + x.project + '/experiment/' + x.name}>{x.name}</Link>,
        props: {
          component: 'th',
          text: x.name,
        },
      },
      {
        title: <Link to={'/project/' + x.project}>{x.project}</Link>,
        props: {
          component: 'th',
          text: x.project,
        },
      },
      x.accessMode,
      x.creator,
    ]);
  };

  const header = (
    <PageSection>
      <Split>
        <SplitItem>
          <Title headingLevel="h1" size="lg">
            Experiments
          </Title>
        </SplitItem>
        <SplitItem isFilled />
        <SplitItem>
          <Button variant="control" aria-label={viewLabel} onClick={toggleView}>
            {viewLabel}
          </Button>
          <Button variant="control" aria-label="New Experiment" onClick={() => setIsModalOpen(true)}>
            New Experiment
          </Button>
        </SplitItem>
      </Split>
    </PageSection>
  );

  const crumbs = (
    <PageSection>
      <Breadcrumb>
        <BreadcrumbItem>Experiments</BreadcrumbItem>
      </Breadcrumb>
    </PageSection>
  );

  const toggleModal = () => {
    setIsModalOpen(!isModalOpen);
    setReload(!reload);
  };

  let url = conf.api + '/experiment';
  if (userView === false) {
    url += '?filter=ByAll';
  }

  return (
    <React.Fragment>
      <NewExperiment isOpen={isModalOpen} onClose={toggleModal} />
      {crumbs}
      {header}
      <PageSection>
        <ActionList kind="Experiment" columns={columns} url={url} actions={actions} mapper={mapper} reload={reload} />
      </PageSection>
    </React.Fragment>
  );
};

type NewExperimentProps = {
  isOpen: boolean;
  onClose: () => void;
};

const NewExperiment: React.FunctionComponent<NewExperimentProps> = (props) => {
  const user = useContext(UserContext);
  const conf = useContext(GeneralSettingsContext);

  const [xp, setXp] = React.useState({ value: '', valid: 'error' });
  const [invalidXp, setInvalidXp] = React.useState('');
  const [pid, setPid] = React.useState({ value: '', valid: 'error' });
  // const [maintainers, setMaintainers] = React.useState([]);
  const [desc, setDesc] = React.useState('');

  const submitForm = () => {
    fetch(conf.api + '/project/' + pid.value + '/experiment/' + xp.value, {
      method: 'PUT',
      credentials: 'include',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        experiment: {
          name: xp.value,
          project: pid.value,
          accessMode: 'Public',
          creator: user.username,
          // maintainers: maintainers,
        },
      }),
    })
      .then((resp) => {
        props.onClose();
        setXp({ value: '', valid: 'error' });
        setPid({ value: '', valid: 'error' });
        setDesc('');
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const onPidSelect = (val) => {
    setPid({ value: val, valid: val === '' ? 'error' : 'success' });
  };

  const onXpChange = (val) => {
    const re = /^[a-z]([a-z0-9]*[a-z0-9])?$/;
    if (re.test(val) === false) {
      setXp({ value: val, valid: 'error' });
      setInvalidXp(
        'The experiment name must start with a lowercase and only contain lowercase alphanumeric characters'
      );
    } else {
      setInvalidXp('');
      setXp({ value: val, valid: val === '' ? 'error' : 'success' });
    }
  };

  // const onMaintainersSelect = (ms) => {
  //     setMaintainers(ms)
  // }

  function mapProj(json: any): Array<string> {
    return json.projects.map((p) => p.name);
  }

  return (
    <Modal isOpen={props.isOpen} onClose={props.onClose} variant={ModalVariant.medium} aria-label="New Experiment">
      <React.Fragment>
        <Title headingLevel="h1" size="2xl">
          New Experiment
        </Title>
        <Form isHorizontal>
          {(xp.valid !== 'success' || pid.valid !== 'success') && (
            <FormAlert>
              <Alert variant="danger" title="All required fields must be filled" aria-live="polite" isInline />
            </FormAlert>
          )}
          <FormGroup fieldId="project" label="Project" validated={pid.valid} isRequired={true}>
            <FetchSelect label="Project" url={conf.api + '/project'} onSelect={onPidSelect} mapItems={mapProj} />
          </FormGroup>
          <FormGroup fieldId="creator" label="Creator" isRequired={true}>
            <TextInput value={user.username} aria-label="Creator" isDisabled />
          </FormGroup>
          {/*
                    TODO - fix FetchMultiSelect.
                    <FormGroup fieldId="maintainers" label="Maintainers">
                        {pid.valid === 'success' ? (
                            <FetchSelectMulti
                                url={conf.api+"/projects/"+pid.value+"/members"}
                                label="Maintainers"
                                mapItems={(json) => json} // should just be array of strings.
                                onSelect={onMaintainersSelect}
                            />
                            ) : (
                                <TextInput value={"Choose Project"} isDisabled />
                            )
                        }
                    </FormGroup>
                    */}
          <FormGroup
            fieldId="name"
            label="Name"
            validated={xp.valid}
            isRequired={true}
            helperText={<FormHelperText isHidden={xp.valid !== 'success'} />}
            helperTextInvalid={invalidXp}
          >
            <TextInput isRequired type="text" id="name" value={xp.value} onChange={(e) => onXpChange(e)} />
          </FormGroup>
          <FormGroup fieldId="desc" label="Description">
            <TextArea id="desc" value={desc} onChange={(e) => setDesc(e)} />
          </FormGroup>
          <FormGroup fieldId="submit">
            <Button
              variant="primary"
              onClick={submitForm}
              isActive={xp.valid !== 'error' && pid.valid !== 'error'}
              isAriaDisabled={xp.valid === 'error' || pid.valid === 'error'}
            >
              Submit
            </Button>
          </FormGroup>
        </Form>
      </React.Fragment>
    </Modal>
  );
};

export { Experiments };
