import * as React from 'react';
import { CubesIcon } from '@patternfly/react-icons';
import {
  PageSection,
  Title,
  Button,
  EmptyState,
  EmptyStateVariant,
  EmptyStateIcon,
  EmptyStateBody,
  EmptyStateSecondaryActions,
} from '@patternfly/react-core';

export interface ISupportProps {
  sampleProp?: string;
}

const Support: React.FunctionComponent<ISupportProps> = () => (
  <PageSection>
    <EmptyState variant={EmptyStateVariant.full}>
      <EmptyStateIcon icon={CubesIcon} />
      <Title headingLevel="h1" size="lg">
        MergeTB Support
      </Title>
      <EmptyStateBody>The primary means of Merge support is through the chat and gitlab issues.</EmptyStateBody>
      <EmptyStateSecondaryActions>
        <Button component="a" href="https://mergetb.org/docs/experimentation" target="blank" variant="link">
          Documentation
        </Button>
        <Button component="a" href="https://chat.mergetb.net/mergetb" target="blank" variant="link">
          Chat
        </Button>
        <Button component="a" href="https://gitlab.com/mergetb" target="blank" variant="link">
          Source Code
        </Button>
        <Button component="a" href="https://gitlab.com/groups/mergetb/-/issues" target="blank" variant="link">
          MergeTB Issues
        </Button>
        <Button component="a" href="https://gitlab.com/mergetb/portal/launch/-/issues" target="blank" variant="link">
          Launch UI Issues
        </Button>
      </EmptyStateSecondaryActions>
    </EmptyState>
  </PageSection>
);

export { Support };
