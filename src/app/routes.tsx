import * as React from 'react';
import { Route, RouteComponentProps, Switch } from 'react-router-dom';
import { accessibleRouteChangeHandler } from '@app/utils/utils';
import { GeneralSettings } from '@app/Settings/General/GeneralSettings';
import { ProfileSettings } from '@app/Settings/Profile/ProfileSettings';
import { NotFound } from '@app/NotFound/NotFound';
import { useDocumentTitle } from '@app/utils/useDocumentTitle';
import { LastLocationProvider, useLastLocation } from 'react-router-last-location';

import { Dashboard } from '@app/Dashboard/Dashboard';
import { Support } from '@app/Support/Support';

import { News } from '@app/News/News';
import { User } from '@app/User/User';
import { Users } from '@app/Users/Users';
import { Projects } from '@app/Projects/Projects';
import { Project } from '@app/Project/Project';
import { Experiments } from '@app/Experiments/Experiments';
import { Experiment } from '@app/Experiment/Experiment';
import { Revision } from '@app/Revision/Revision';
import { Models } from '@app/Models/Models';
//import { Facilities } from '@app/Facilities/Facilities';
//import { Pools } from '@app/Pools/Pools';
import { Realization } from '@app/Realization/Realization';
import { Realizations } from '@app/Realizations/Realizations';
import { Materializations } from '@app/Materializations/Materializations';
import { Materialization } from '@app/Materialization/Materialization';
import { XDCs } from '@app/XDCs/XDCs';

let routeFocusTimer: number;
export interface IAppRoute {
  label?: string; // Excluding the label will exclude the route from the nav sidebar in AppLayout
  /* eslint-disable @typescript-eslint/no-explicit-any */
  component: React.ComponentType<RouteComponentProps<any>> | React.ComponentType<any>;
  /* eslint-enable @typescript-eslint/no-explicit-any */
  exact?: boolean;
  path: string;
  title: string;
  isAsync?: boolean;
  routes?: undefined;
}

export interface IAppRouteGroup {
  label: string;
  routes: IAppRoute[];
}

export type AppRouteConfig = IAppRoute | IAppRouteGroup;

const routes: AppRouteConfig[] = [
  /*
  {
    component: MergeLogin,
    exact: true,
    path: '/login',
    title: 'Merge Portal | Login',
  },
  */
  {
    component: Dashboard,
    exact: true,
    path: '/',
    title: '',
  },
  {
    component: News,
    exact: true,
    isAsync: true,
    label: 'News',
    path: '/news',
    title: 'Merge Portal | News',
  },
  {
    component: Users,
    exact: true,
    isAsync: true,
    label: 'Users',
    path: '/user',
    title: 'Merge Portal | Users',
  },
  {
    component: User,
    exact: true,
    isAsync: true,
    path: '/user/:uid',
    title: 'Merge Portal',
  },
  {
    component: Projects,
    exact: true,
    isAsync: true,
    label: 'Projects',
    path: '/project',
    title: 'Merge Portal | Projects',
  },
  {
    component: Project,
    exact: true,
    path: '/project/:pid',
    title: 'Merge Portal',
  },
  /*
  {
    component: Identity,
    exact: true,
    isAsync: true,
    label: 'Identity',
    path: '/identity',
    title: 'Merge Portal | Identity Management',
  },
  */
  {
    component: Experiments,
    exact: true,
    isAsync: true,
    label: 'Experiments',
    path: '/experiment',
    title: 'Merge Portal | Experiment Management',
  },
  {
    component: Experiment,
    exact: true,
    isAsync: true,
    path: '/project/:pid/experiment/:eid',
    title: 'Merge Portal',
  },
  {
    component: Revision,
    exact: true,
    isAsync: true,
    path: '/model/:pid/:eid/:rev',
    title: 'Merge Portal',
  },
  /*
  {
    component: Pools,
    exact: true,
    isAsync: true,
    label: 'Pools',
    path: '/pools',
    title: 'Merge Portal | Pool Management',
  },
  {
    component: Facilities,
    exact: true,
    isAsync: true,
    label: 'Facilities',
    path: '/facilities',
    title: 'Merge Portal | Facility Management',
  },
  */
  {
    component: Realizations,
    exact: true,
    isAsync: true,
    label: 'Realizations',
    path: '/realizations',
    title: 'Merge Portal | Realization Management',
  },
  {
    component: Realization,
    exact: true,
    isAsync: true,
    path: '/realizations/:pid/:eid/:rid',
    title: 'Merge Portal',
  },
  {
    component: Materializations,
    exact: true,
    isAsync: true,
    label: 'Materializations',
    path: '/materializations',
    title: 'Merge Portal | Materialization Management',
  },
  {
    component: Materialization,
    exact: true,
    isAsync: true,
    path: '/materializations/:pid/:eid/:rid',
    title: 'Merge Portal',
  },
  {
    component: XDCs,
    exact: true,
    isAsync: true,
    label: 'XDCs',
    path: '/xdcs',
    title: 'Merge Portal | XDC Management',
  },
  {
    component: Models,
    exact: true,
    isAsync: true,
    label: 'Model Editor',
    path: '/models',
    title: 'Merge Portal | Model Editor',
  },
  {
    component: Support,
    exact: true,
    isAsync: true,
    label: 'Support',
    path: '/support',
    title: 'Merge Portal | Support Page',
  },
  {
    label: 'Settings',
    routes: [
      {
        component: GeneralSettings,
        exact: true,
        label: 'General',
        path: '/settings/general',
        title: 'Merge Portal | General Settings',
      },
      {
        component: ProfileSettings,
        exact: true,
        label: 'Profile',
        path: '/settings/profile',
        title: 'Merge Portal | Profile Settings',
      },
    ],
  },
];

// a custom hook for sending focus to the primary content container
// after a view has loaded so that subsequent press of tab key
// sends focus directly to relevant content
const useA11yRouteChange = (isAsync: boolean) => {
  const lastNavigation = useLastLocation();
  React.useEffect(() => {
    if (!isAsync && lastNavigation !== null) {
      routeFocusTimer = accessibleRouteChangeHandler();
    }
    return () => {
      window.clearTimeout(routeFocusTimer);
    };
  }, [isAsync, lastNavigation]);
};

const RouteWithTitleUpdates = ({ component: Component, isAsync = false, title, ...rest }: IAppRoute) => {
  useA11yRouteChange(isAsync);
  useDocumentTitle(title);

  function routeWithTitle(routeProps: RouteComponentProps) {
    return <Component {...rest} {...routeProps} />;
  }

  return <Route render={routeWithTitle} {...rest} />;
};

const PageNotFound = ({ title }: { title: string }) => {
  useDocumentTitle(title);
  return <Route component={NotFound} />;
};

const flattenedRoutes: IAppRoute[] = routes.reduce(
  (flattened, route) => [...flattened, ...(route.routes ? route.routes : [route])],
  [] as IAppRoute[]
);

const AppRoutes = (): React.ReactElement => (
  <LastLocationProvider>
    <Switch>
      {flattenedRoutes.map(({ path, exact, component, title, isAsync }, idx) => (
        <RouteWithTitleUpdates
          path={path}
          exact={exact}
          component={component}
          key={idx}
          title={title}
          isAsync={isAsync}
        />
      ))}
      <PageNotFound title="404 Page Not Found" />
    </Switch>
  </LastLocationProvider>
);

export { AppRoutes, routes };
