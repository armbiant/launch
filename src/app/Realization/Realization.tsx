import * as React from 'react';
import { useParams } from 'react-router-dom';
import { GeneralSettingsContext } from '@app/Settings/General/GeneralSettings';
import { useFetch } from 'use-http';

import {
  PageSection,
  Breadcrumb,
  BreadcrumbItem,
  Card,
  CardHeader,
  CardBody,
  Spinner,
  Bullseye,
  Alert,
  Text,
} from '@patternfly/react-core';

import { headerCol, sortable } from '@patternfly/react-table';
import { ActionList } from '@app/lib/ActionList';

const Realization: React.FunctionComponent = () => {
  const { pid, eid, rid } = useParams();
  const conf = React.useContext(GeneralSettingsContext);

  const options = { credentials: 'include' };
  const { loading, error, data } = useFetch(
    conf.api + '/realize/realizations/' + pid + '/' + eid + '/' + rid,
    options,
    []
  );

  const crumbs = (
    <PageSection>
      <Breadcrumb>
        <BreadcrumbItem to="/project">Projects</BreadcrumbItem>
        <BreadcrumbItem to={'/project/' + pid}>{pid}</BreadcrumbItem>
        <BreadcrumbItem to="/experiment">Experiments</BreadcrumbItem>
        <BreadcrumbItem to={'/project/' + pid + '/experiment/' + eid}>{eid}</BreadcrumbItem>
        <BreadcrumbItem to="/realizations">Realizations</BreadcrumbItem>
        <BreadcrumbItem>{rid}</BreadcrumbItem>
      </Breadcrumb>
    </PageSection>
  );

  return (
    <React.Fragment>
      {crumbs}
      <PageSection>
        <Card id={rid + '-card-id'}>
          <CardHeader id={rid + '-cardheader-id'}>Details</CardHeader>
          <CardBody>
            {error && !data && (
              <Alert variant="danger" title="Error">
                Error loading
              </Alert>
            )}
            {error && data && data.hasOwnProperty('message') && (
              <Alert variant="danger" title="Response Error">
                <pre>{JSON.stringify(data, null, 2)}</pre>
              </Alert>
            )}
            {loading && (
              <Bullseye>
                <Spinner size="sm" />
              </Bullseye>
            )}
            {data && data.hasOwnProperty('result') && (
              <>
                <RealizationNodes rlz={data.result.realization} active={false} />
              </>
            )}
          </CardBody>
        </Card>
      </PageSection>
    </React.Fragment>
  );
};

type RlzNodesProps = {
  rlz: unknown;
  active: boolean;
};

const RealizationNodes: React.FunctionComponent<RlzNodesProps> = ({ rlz, active }) => {
  const cells = [
    { title: 'Name', cellTransforms: [headerCol()], transforms: [sortable] },
    { title: 'Experiment Addresses' },
    { title: 'Infranet Address' },
    { title: 'Kind' },
    { title: 'Resource' },
    { title: 'Image' },
    { title: 'Facility' },
  ];

  const formatAddrs = (sockets) => {
    let val = [];
    sockets.forEach((s) => {
      val = [...val, ...s.addrs];
    });
    return val.join(', ');
  };

  const rows = Object.keys(rlz.nodes).map((n) => [
    rlz.nodes[n].node.id,
    formatAddrs(rlz.nodes[n].node.sockets),
    rlz.nodes[n].infranetAddr,
    rlz.nodes[n].kind,
    rlz.nodes[n].resource.id,
    rlz.nodes[n].node.image.value,
    rlz.nodes[n].facility,
  ]);

  const actions = [
    { title: 'Console', disabled: true },
    { title: 'Reboot', disabled: true },
  ];

  return <ActionList kind="Realization" columns={cells} rows={rows} actions={active ? actions : []} />;
};

export { Realization, RealizationNodes };
