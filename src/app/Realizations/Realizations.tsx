import * as React from 'react';
import {
  PageSection,
  Title,
  Breadcrumb,
  BreadcrumbItem,
  Alert,
  AlertGroup,
  AlertActionCloseButton,
  Split,
  SplitItem,
  Popover,
  Button,
} from '@patternfly/react-core';
import { Link } from 'react-router-dom';
import { ActionList } from '@app/lib/ActionList';
import { sortable, headerCol } from '@patternfly/react-table';
import { GeneralSettingsContext } from '@app/Settings/General/GeneralSettings';
import { UserContext } from '@app/User/User';

const Realizations: React.FunctionComponent = () => {
  var userview_last = (localStorage.getItem('userview') ?? true) == true;

  const conf = React.useContext(GeneralSettingsContext);
  const [reload, setReload] = React.useState(0);
  const [alerts, setAlerts] = React.useState([]);
  const idTraits = React.useContext(UserContext);

  const [viewLabel, setViewLabel] = React.useState('View ' + (userview_last ? 'All' : 'Own'));
  const [userView, setUserView] = React.useState(userview_last);

  const columns = [
    { title: 'Realization', cellTransforms: [headerCol()], transforms: [sortable] },
    { title: 'Experiment', transforms: [sortable] },
    { title: 'Project', cellTransforms: [headerCol()], transforms: [sortable] },
    { title: 'Creator', cellTransforms: [headerCol()], transforms: [sortable] },
    { title: 'Revision' },
    { title: 'Succeeded' },
    { title: 'Nodes' },
    { title: 'Links' },
  ];

  const mapper = (json) =>
    json.results.map((x) => [
      {
        title: (
          <Link to={'/realizations/' + x.realization.pid + '/' + x.realization.eid + '/' + x.realization.id}>
            {x.realization.id}
          </Link>
        ),
        props: {
          component: 'th',
          text: x.realization.id,
        },
      },
      {
        title: (
          <Link to={'/project/' + x.realization.pid + '/experiment/' + x.realization.eid}>{x.realization.eid}</Link>
        ),
        props: {
          component: 'th',
          text: x.realization.eid,
        },
      },
      {
        title: <Link to={'/project/' + x.realization.pid}>{x.realization.pid}</Link>,
        props: {
          component: 'th',
          text: x.realization.pid,
        },
      },
      {
        title: <Link to={'/user/' + x.realization.creator}>{x.realization.creator}</Link>,
        props: {
          component: 'th',
          text: x.realization.creator,
        },
      },
      {
        title: (
          <Link to={'/model/' + x.realization.pid + '/' + x.realization.eid + '/' + x.realization.xhash}>
            {x.realization.xhash.substring(0, 8)}
          </Link>
        ),
        props: {
          component: 'th',
          text: x.realization.xhash.substring(0, 8),
        },
      },
      Object.keys(x.diagnostics.value).length === 0 ? (
        'Yes'
      ) : (
        <React.Fragment>
          <Popover
            aria-label="Realization Diagnostics"
            headerContent={<div>Realization Diagnostics</div>}
            bodyContent={JSON.stringify(x.diagnostics.value)}
          >
            <Button variant="danger">Failed</Button>
          </Popover>
        </React.Fragment>
      ),
      Object.keys(x.realization.nodes).length,
      Object.keys(x.realization.links).length,
    ]);

  const actions = [
    {
      title: 'Relinquish',
      onClick: (event, rowId, rowData, extra) => {
        const rid = rowData[0].props.text;
        const eid = rowData[1].props.text;
        const pid = rowData[2].props.text;

        fetch(conf.api + '/realize/realizations/' + pid + '/' + eid + '/' + rid, {
          method: 'DELETE',
          credentials: 'include',
        })
          .then((response) => {
            if (response.ok) {
              return response.json();
            } else {
              return (
                response.text(),
                then((text) => {
                  throw new Error(text);
                })
              );
            }
          })
          .then((json) => {
            addAlert('Realization ' + rid + '.' + eid + '.' + pid + ' relinquished.', '', 'success');
            setReload(reload + 1);
          })
          .catch((error) => {
            console.log('got error:', error);
            addAlert('Relinquish Error', error.message, 'danger');
          });
      },
    },
    {
      title: 'Materialize',
      onClick: (event, rowId, rowData) => {
        const rid = rowData[0].props.text;
        const eid = rowData[1].props.text;
        const pid = rowData[2].props.text;

        fetch(conf.api + '/materialize/materialize/' + pid + '/' + eid + '/' + rid, {
          method: 'PUT',
          credentials: 'include',
        })
          .then((response) => {
            if (response.ok) {
              return response.json();
            } else {
              return response.text().then((text) => {
                throw new Error(text);
              });
            }
          })
          .then((json) => {
            addAlert('Materialization ' + rid + '.' + eid + '.' + pid + ' started.', '', 'success');
            setReload(reload + 1);
          })
          .catch((error) => {
            console.log('got error:', error);
            addAlert('Materialize Error', error.message, 'danger');
          });
      },
    },
  ];

  const toggleView = () => {
    setViewLabel('View ' + (userView === false ? 'All' : 'Own'));
    localStorage.setItem('userview', userView ? '0' : '1');
    setUserView(!userView);
    setReload(!reload);
  };

  const crumbs = (
    <PageSection>
      <Breadcrumb>
        <BreadcrumbItem>User</BreadcrumbItem>
        <BreadcrumbItem to={'/users/' + idTraits.username}>{idTraits.username}</BreadcrumbItem>
        <BreadcrumbItem>Realizations</BreadcrumbItem>
      </Breadcrumb>
    </PageSection>
  );

  const header = (
    <PageSection>
      <Split>
        <SplitItem>
          <Title headingLevel="h1" size="lg">
            Realizations
          </Title>
        </SplitItem>
        <SplitItem isFilled />
        <SplitItem>
          <Button variant="control" aria-label={viewLabel} onClick={toggleView}>
            {viewLabel}
          </Button>
        </SplitItem>
      </Split>
    </PageSection>
  );

  const addAlert = (t, m, v) => {
    setAlerts((prev) => [...prev, { title: t, message: m, variant: v, key: new Date().getTime() }]);
  };

  const removeAlert = (key) => {
    setAlerts([...alerts.filter((el) => el.key !== key)]);
  };

  const notifications = (
    <AlertGroup isToast>
      {alerts.map((a, i) => (
        <Alert
          isExpandable={a.message !== ''}
          variant={a.variant}
          title={a.title}
          key={a.key}
          timeout={8000}
          actionClose={
            <AlertActionCloseButton
              title={a.title}
              variantLabel={`${a.variant} alert`}
              onClose={() => removeAlert(a.key)}
            />
          }
        >
          {a.message}
        </Alert>
      ))}
    </AlertGroup>
  );

  let url = conf.api + '/realize/realizations';
  if (userView === false) {
    url += '?filter=ByAll';
  }

  return (
    <React.Fragment>
      {alerts.length !== 0 && notifications}
      {crumbs}
      {header}
      <PageSection>
        <ActionList kind="Realizations" columns={columns} url={url} actions={actions} mapper={mapper} reload={reload} />
      </PageSection>
    </React.Fragment>
  );
};

export { Realizations };
