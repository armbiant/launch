import * as React from 'react';
import { useContext } from 'react';
import { PageSection, PageSectionVariants, Title, TextContent, TextList, TextListItem } from '@patternfly/react-core';
import { UserContext } from '@app/User/User';

const ProfileSettings: React.FunctionComponent = () => {
  // Ideally here, we'd get follow and parse the identity traits that is
  // given to us from Kratos and display the information here generically.
  const idTraits = useContext(UserContext);

  return (
    <React.Fragment>
      <PageSection variant={PageSectionVariants.light}>
        <Title headingLevel="h1" size="lg">
          Profile Settings
        </Title>
      </PageSection>
      <PageSection variant={PageSectionVariants.darker}>
        <TextContent>
          <TextList component="dl">
            <TextListItem component="dt">Username</TextListItem>
            <TextListItem component="dd">{idTraits.username}</TextListItem>
            <TextListItem component="dt">Email Address</TextListItem>
            <TextListItem component="dd">{idTraits.email}</TextListItem>
          </TextList>
        </TextContent>
      </PageSection>
      <PageSection variant={PageSectionVariants.light}></PageSection>
    </React.Fragment>
  );
};

export { ProfileSettings };
