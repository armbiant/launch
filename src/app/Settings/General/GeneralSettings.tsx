import * as React from 'react';
import { env } from '@app/env.js';
import {
  PageSection,
  PageSectionVariants,
  Title,
  TextContent,
  TextList,
  TextListItem,
  Form,
  FormGroup,
  FormSelect,
  FormSelectOption,
} from '@patternfly/react-core';

type GenConfType = {
  portal: string;
  api: string;
  auth: string;
  testbeds: {};
};

let Gconf: GenConfType = {
  portal: env.REACT_APP_MERGE_PORTAL,
  api: env.REACT_APP_MERGE_API_URI,
  auth: env.REACT_APP_MERGE_AUTH_URI,
  testbeds: {},
};

const GeneralSettingsContext = React.createContext<GenConfType>(Gconf);

const GeneralSettings: React.FunctionComponent = () => {
  const conf = React.useContext(GeneralSettingsContext);

  console.log('conf', conf);

  return (
    <React.Fragment>
      <PageSection variant={PageSectionVariants.light}>
        <Title headingLevel="h1" size="lg">
          General Settings
        </Title>
      </PageSection>
      {Object.keys(conf.testbeds).length !== 0 && (
        <PageSection>
          <Form>
            <FormGroup label="Merge Portal" fieldId="merge-portal" helperText="Choose the Merge Portal to connect to">
              <FormSelect
                value={conf.portal}
                onChange={(v) => conf.update(v, conf)}
                id="select-portal"
                name="select-portal"
                aria-label="Merge Portal"
              >
                {Object.keys(conf.testbeds).map((name, i) => (
                  <FormSelectOption isDisabled={false} key={i} value={name} label={name} />
                ))}
              </FormSelect>
            </FormGroup>
          </Form>
        </PageSection>
      )}
      <PageSection variant={PageSectionVariants.darker}>
        <TextContent>
          <TextList component="dl">
            <TextListItem component="dt">Merge Portal</TextListItem>
            <TextListItem component="dd">{conf.portal}</TextListItem>
            <TextListItem component="dt">Merge API Endpoint</TextListItem>
            <TextListItem component="dd">{conf.api}</TextListItem>
            <TextListItem component="dt">Authentication Endpoint</TextListItem>
            <TextListItem component="dd">{conf.auth}</TextListItem>
          </TextList>
        </TextContent>
      </PageSection>
      <PageSection variant={PageSectionVariants.light}></PageSection>
    </React.Fragment>
  );
};

export { GeneralSettings, GeneralSettingsContext, GenConfType, Gconf };
