import * as React from 'react';
import { useParams } from 'react-router-dom';
import { GeneralSettingsContext } from '@app/Settings/General/GeneralSettings';
import { useFetch } from 'use-http';
import { LocalizeDate } from '@app/lib/TaskStatusUtils';

import {
  PageSection,
  Breadcrumb,
  BreadcrumbItem,
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Tabs,
  Tab,
  TabTitleText,
  Spinner,
  Bullseye,
  Alert,
  Form,
  FormGroup,
  TextInput,
  TextArea,
} from '@patternfly/react-core';

import { headerCol, sortable } from '@patternfly/react-table';
import { ActionList } from '@app/lib/ActionList';
import { TaskStatusTable } from '@app/lib/TaskStatusTable';

const Materialization: React.FunctionComponent = () => {
  const { pid, eid, rid } = useParams();
  const conf = React.useContext(GeneralSettingsContext);
  const [activeTab, setActiveTab] = React.useState(0);

  const options = { credentials: 'include' };

  const st_url = conf.api + '/materialize/status/' + pid + '/' + eid + '/' + rid;
  const mz_url = conf.api + '/materialize/materializations/' + pid + '/' + eid + '/' + rid;

  const { loading, error, data } = useFetch(mz_url, options, []);

  const crumbs = (
    <PageSection>
      <Breadcrumb>
        <BreadcrumbItem to="/project">Projects</BreadcrumbItem>
        <BreadcrumbItem to={'/project/' + pid}>{pid}</BreadcrumbItem>
        <BreadcrumbItem to="/experiment">Experiments</BreadcrumbItem>
        <BreadcrumbItem to={'/project/' + pid + '/experiment/' + eid}>{eid}</BreadcrumbItem>
        <BreadcrumbItem to="/realizations">Realizations</BreadcrumbItem>
        <BreadcrumbItem to={'/realizations/' + pid + '/' + eid + '/' + rid}>{rid}</BreadcrumbItem>
        <BreadcrumbItem to="/materializations">Materializations</BreadcrumbItem>
        <BreadcrumbItem>
          {rid}.{eid}.{pid}
        </BreadcrumbItem>
      </Breadcrumb>
    </PageSection>
  );

  const tabClick = (event, index) => {
    setActiveTab(index);
  };

  const getter = (data) => {
    return data.status;
  };

  return (
    <React.Fragment>
      {crumbs}
      <PageSection>
        <Card id={rid + '-card-id'}>
          <CardBody>
            {error && !data && (
              <Alert variant="danger" title="Error">
                Error loading
              </Alert>
            )}
            {error && data && data.hasOwnProperty('message') && (
              <Alert variant="danger" title="Response Error">
                <pre>{JSON.stringify(data, null, 2)}</pre>
              </Alert>
            )}
            {loading && (
              <Bullseye>
                <Spinner size="sm" />
              </Bullseye>
            )}
            {data && data.hasOwnProperty('materialization') && (
              <div>
                <Tabs activeKey={activeTab} onSelect={tabClick}>
                  <Tab eventKey={0} title={<TabTitleText>Status</TabTitleText>}>
                    <TaskStatusTable
                      kind={rid + '-tst'}
                      url={st_url}
                      getter={getter}
                      ongoingfrequency={5000}
                      completedfrequency={30000}
                      scalingfrequency={1.0 / 10.0}
                    />
                  </Tab>
                  <Tab eventKey={1} title={<TabTitleText>Details</TabTitleText>}>
                    <MaterializationNodes data={data} active={false} />
                  </Tab>
                </Tabs>
              </div>
            )}
          </CardBody>
        </Card>
      </PageSection>
    </React.Fragment>
  );
};

type MtzNodesProps = {
  data: unknown;
  active: boolean;
};

const MaterializationNodes: React.FunctionComponent<MtzNodesProps> = ({ data, active }) => {
  var mtz = data.materialization;
  var status = data.status;

  const cells = [
    { title: 'Name', cellTransforms: [headerCol()], transforms: [sortable] },
    { title: 'Experiment Addresses' },
    { title: 'Infranet Address' },
    { title: 'Kind' },
    { title: 'Resource' },
    { title: 'Image' },
    { title: 'Facility' },
  ];

  const formatAddrs = (sockets) => {
    let val = [];
    sockets.forEach((s) => {
      val = [...val, ...s.addrs];
    });
    return val.join(', ');
  };

  const metals = mtz.metal.map((x) => [
    x.model.id,
    formatAddrs(x.model.sockets),
    x.infranetAddr,
    'BareMetal',
    x.resource,
    x.model.image.value,
    x.facility,
  ]);

  const vms = mtz.vms.map((x) => [
    x.vmAlloc.node,
    formatAddrs(x.vmAlloc.model.sockets),
    x.infranetAddr,
    'VirtualMachine',
    x.vmAlloc.resource,
    x.vmAlloc.model.image.value,
    x.vmAlloc.facility,
  ]);

  const rows = metals.concat(vms);

  const actions = [
    { title: 'Console', disabled: true },
    { title: 'Reboot', disabled: true },
  ];

  const details = (
    <Card id="deetsCard">
      <CardHeader>
        <CardTitle id="deetsCardTitle">Details</CardTitle>
      </CardHeader>
      <CardBody>
        <Form isHorizontal>
          <FormGroup fieldId="Created" label="Created">
            <TextInput value={LocalizeDate(status.FirstUpdated)} aria-label="Created" isDisabled />
          </FormGroup>
        </Form>
      </CardBody>
    </Card>
  );

  const nodes = (
    <Card>
      <CardHeader>
        <CardTitle id="nodesTitles">Nodes</CardTitle>
      </CardHeader>
      <CardBody>
        <ActionList kind="Materialization" columns={cells} rows={rows} actions={active ? actions : []} />
      </CardBody>
    </Card>
  );

  return (
    <React.Fragment>
      {details}
      {nodes}
    </React.Fragment>
  );
};

export { Materialization };
