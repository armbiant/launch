import * as React from 'react';
import {
  Alert,
  AlertGroup,
  AlertVariant,
  PageSection,
  Title,
  Form,
  FormGroup,
  FormAlert,
  FormHelperText,
  ActionGroup,
  Button,
  TextInput,
  Split,
  SplitItem,
  Modal,
  ModalVariant,
  Breadcrumb,
  BreadcrumbItem,
} from '@patternfly/react-core';
import { ActionList } from '@app/lib/ActionList';
import { Link } from 'react-router-dom';
import { sortable, headerCol } from '@patternfly/react-table';
import { FetchSelect } from '@app/lib/FetchSelect';
import { GeneralSettingsContext } from '@app/Settings/General/GeneralSettings';

const XDCs: React.FunctionComponent = () => {
  const [reload, setReload] = React.useState(false);

  const conf = React.useContext(GeneralSettingsContext);
  const [isModalOpen, setIsModalOpen] = React.useState(false);
  const [xdc, setXdc] = React.useState({ value: '', valid: 'error' });
  const [invalidXDC, setInvalidXDC] = React.useState('');
  const [pid, setPid] = React.useState({ value: '', valid: 'error' });
  const [alerts, setAlerts] = React.useState([]);

  // attach dialog
  const [attachIsOpen, setAttachIsOpen] = React.useState(false);
  const [attachTo, setAttachTo] = React.useState('');
  const [attachXDC, setAttachXDC] = React.useState('');

  const columns = [
    { title: 'Name', cellTransforms: [headerCol()], transforms: [sortable] },
    { title: 'Project', cellTransforms: [headerCol()], transforms: [sortable] },
    { title: 'Attached', cellTransforms: [headerCol()], transforms: [sortable] },
    { title: 'URL', cellTransforms: [], transforms: [sortable] },
    { title: 'FQDN', cellTransforms: [headerCol()], transforms: [sortable] },
    { title: 'Creator', cellTransforms: [headerCol()], transforms: [sortable] },
    { title: 'Memory Limit', cellTransforms: [headerCol()], transforms: [sortable] },
    { title: 'CPU Limit', cellTransforms: [headerCol()], transforms: [sortable] },
    { title: 'Image', cellTransforms: [headerCol()], transforms: [sortable] },
  ];

  const actions = [
    {
      title: 'Attach',
      onClick: (event, rowId, rowData) => {
        setAttachXDC(rowData.name.title + '.' + rowData.project.title.props.text);
        setAttachIsOpen(true);
      },
    },
    {
      title: 'Detach',
      onClick: (event, rowId, rowData) => {
        const xdc = rowData.name.title;
        const mtz = rowData.attached.title.props.text;
        const [rid, eid, pid] = mtz.split('.');

        fetch(conf.api + '/xdc/attach/' + xdc + '/' + pid, {
          method: 'DELETE',
          credentials: 'include',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            experiment: eid,
            realization: rid,
          }),
        })
          .then((resp) => {
            if (resp.ok) {
              setReload(!reload);
              addAlert('XDC ' + xdc + ' detached from ' + mtz, 'success');
            }
          })
          .catch((error) => {
            addAlert(error.message, 'danger');
          });
      },
    },
    {
      title: 'Delete',
      onClick: (event, rowId, rowData) => {
        const xdc = rowData.name.title;
        const pid = rowData.project.title.props.text;
        fetch(conf.api + '/xdc/instance/' + xdc + '/' + pid, {
          method: 'DELETE',
          credentials: 'include',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            xdc: xdc,
            project: pid,
          }),
        })
          .then((resp) => {
            if (resp.ok) {
              setReload(!reload);
              addAlert('XDC deleted: ' + xdc, 'success');
            }
          })
          .catch((error) => {
            console.log(error);
            addAlert(error.message, 'danger');
          });
      },
    },
  ];

  let mapper = (json) => {
    return json.XDCs.map((x) => {
      const [xid, pid] = x.name.split('.');
      const [mrid, meid, mpid] = x.materialization.split('.');
      return [
        xid,
        {
          title: (
            <Link to={'/project/' + pid} text={pid}>
              {pid}
            </Link>
          ),
        },
        {
          title: (
            <Link to={'/materializations/' + mpid + '/' + meid + '/' + mrid} text={x.materialization}>
              {x.materialization}
            </Link>
          ),
        },
        {
          title: (
            <a href={x.url} target="_blank">
              Jupyter
            </a>
          ),
        },
        x.fqdn,
        x.creator,
        x.memlimit,
        x.cpulimit,
        x.image,
      ];
    });
  };

  const addAlert = (t, v) => {
    setAlerts((prev) => [...prev, { title: t, variant: v }]);
  };

  const toggleModal = () => {
    setIsModalOpen(!isModalOpen);
  };

  const submitForm = () => {
    fetch(conf.api + '/xdc/instance/' + xdc.value + '/' + pid.value, {
      method: 'PUT',
      credentials: 'include',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        xdc: xdc.value,
        project: pid.value,
      }),
    })
      .then((response) => {
        if (response.ok) {
          setReload(!reload);
          toggleModal();
          addAlert('XDC created: ' + xdc.value, 'success');
          return;
        } else {
          return response.text().then((text) => {
            throw new Error(text);
          });
        }
      })
      .catch((error) => {
        console.log(error);
        addAlert(error.message, 'danger');
      });
  };

  const submitAttach = () => {
    // "attachXDC" is the xdc name to attach to mtz "attachTo"
    // xdc.project ==> rlz.eid.pidS
    const xdcTkns = attachXDC.split('.');
    const mtzTnks = attachTo.split('.');
    fetch(conf.api + '/xdc/attach/' + xdcTkns[0] + '/' + mtzTnks[2] + '/' + mtzTnks[1] + '/' + mtzTnks[0], {
      method: 'PUT',
      credentials: 'include',
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then((resp) => {
        if (resp.ok) {
          setAttachIsOpen(false);
          setAttachXDC('');
          setAttachTo('');
          setReload(!reload);
          addAlert('XDC ' + attachXDC + ' attached to ' + attachTo, 'success');
          return;
        } else {
          return resp.text().then((text) => {
            throw new Error(text);
          });
        }
      })
      .catch((error) => {
        console.log(error);
        addAlert(error.message, 'danger');
      });
  };

  const onPidSelect = (val) => {
    setPid({ value: val, valid: val === '' ? 'error' : 'success' });
  };

  function mapProj(json: any): Array<string> {
    return json.projects.map((p) => p.name);
  }

  function mapMtzs(json: any): Array<string> {
    const [_, pid] = attachXDC.split('.');
    return json.materializations
      .map((m) => {
        if (m.pid == pid) {
          return m.rid + '.' + m.eid + '.' + m.pid;
        }
        return 'EMPTY';
      })
      .filter((e) => {
        return e !== 'EMPTY';
      }); // there is likely a better way to do this.
  }

  const onXDCChange = (val) => {
    const re = /^[a-z]([a-z0-9]*[a-z0-9])?$/;
    if (re.test(val) === false) {
      setXdc({ value: val, valid: 'error' });
      setInvalidXDC('The XDC name must start with a lowercase and only contain lowercase alphanumeric characters');
    } else {
      setInvalidXDC('');
      setXdc({ value: val, valid: val === '' ? 'error' : 'success' });
    }
  };

  const crumbs = (
    <PageSection>
      <Breadcrumb>
        <BreadcrumbItem>XDCs</BreadcrumbItem>
      </Breadcrumb>
    </PageSection>
  );

  const header = (
    <PageSection>
      <Split>
        <SplitItem>
          <Title headingLevel="h1" size="lg">
            Experiment Development Containers
          </Title>
        </SplitItem>
        <SplitItem isFilled />
        <SplitItem>
          <Button variant="control" aria-label="New XDC" onClick={toggleModal}>
            New XDC
          </Button>
        </SplitItem>
      </Split>
    </PageSection>
  );

  const newModal = (
    <Modal
      isOpen={isModalOpen}
      onClose={toggleModal}
      variant={ModalVariant.medium}
      aria-label="New Experiment Development Container"
    >
      <React.Fragment>
        <Title headingLevel="h1" size="2xl">
          New Experiment Development Container
        </Title>
        <Form isHorizontal>
          {(xdc.valid !== 'success' || pid.valid !== 'success') && (
            <FormAlert>
              <Alert variant="danger" title="All required fields must be filled" aria-live="polite" isInline />
            </FormAlert>
          )}
          <FormGroup fieldId="project" label="Project" validated={pid.valid} isRequired={true}>
            <FetchSelect label="Project" url={conf.api + '/project'} onSelect={onPidSelect} mapItems={mapProj} />
          </FormGroup>
          <FormGroup
            fieldId="xdcname"
            label="Name"
            helperText={<FormHelperText isHidden={xdc.valid !== 'success'} />}
            helperTextInvalid={invalidXDC}
            field-id="name"
            validated={xdc.valid}
          >
            <TextInput isRequired type="text" id="name" value={xdc.value} onChange={(e) => onXDCChange(e)} />
          </FormGroup>
          <ActionGroup>
            <Button variant="primary" onClick={submitForm}>
              Submit
            </Button>
          </ActionGroup>
        </Form>
      </React.Fragment>
    </Modal>
  );

  const attachModel = (
    <Modal
      isOpen={attachIsOpen}
      onClose={() => setAttachIsOpen(!attachIsOpen)}
      variant={ModalVariant.medium}
      aria-label="Attach to Materialization"
    >
      <React.Fragment>
        <Title headingLevel="h1" size="2xl">
          Attach to Materialization
        </Title>
        <Form isHorizontal>
          <FormGroup fieldId="xdcname" label="XDC" isRequired={true}>
            <TextInput isRequired type="text" id="name" value={attachXDC} isDisabled={true} />
          </FormGroup>
          <FormGroup fieldId="mtz" label="Materialization" isRequired={true}>
            <FetchSelect
              label="Materialization"
              url={conf.api + '/materialize/materializations'}
              onSelect={setAttachTo}
              mapItems={mapMtzs}
            />
          </FormGroup>
          <ActionGroup>
            <Button variant="primary" onClick={submitAttach}>
              Attach
            </Button>
          </ActionGroup>
        </Form>
      </React.Fragment>
    </Modal>
  );

  const notifications = (
    <AlertGroup isToast>
      {alerts.map((a, i) => (
        <Alert variant={AlertVariant[a.var]} title={a.title} timeout={9000} key={i} />
      ))}
    </AlertGroup>
  );

  return (
    <React.Fragment>
      {alerts.length !== 0 && notifications}
      {crumbs}
      {newModal}
      {attachModel}
      {header}
      <PageSection>
        <ActionList
          kind="XDC"
          columns={columns}
          url={conf.api + '/xdc/list/'}
          actions={actions}
          mapper={mapper}
          reload={reload}
        />
      </PageSection>
    </React.Fragment>
  );
};

export { XDCs };
