import * as React from 'react';
import { useContext } from 'react';
import {
  Alert,
  AlertGroup,
  AlertVariant,
  PageSection,
  Title,
  Modal,
  ModalVariant,
  Form,
  FormGroup,
  FormHelperText,
  ActionGroup,
  Button,
  TextInput,
  Split,
  SplitItem,
  Breadcrumb,
  BreadcrumbItem,
} from '@patternfly/react-core';
import { ActionList } from '@app/lib/ActionList';
import { sortable, headerCol } from '@patternfly/react-table';
import { Link } from 'react-router-dom';
import { UserContext } from '@app/User/User';
import { GeneralSettingsContext } from '@app/Settings/General/GeneralSettings';

const Projects: React.FunctionComponent = () => {
  var userview_last = (localStorage.getItem('userview') ?? true) == true;

  const user = useContext(UserContext);
  const conf = useContext(GeneralSettingsContext);
  const { username } = user;

  const [showNew, setShowNew] = React.useState(false);
  const [newProjName, setNewProjName] = React.useState({ value: '', valid: 'error' });
  const [newProjDesc, setNewProjDesc] = React.useState('');
  const [alerts, setAlerts] = React.useState([]);
  const [invalidName, setInvalidName] = React.useState('');
  const [reload, setReload] = React.useState(false);

  const [viewLabel, setViewLabel] = React.useState('View ' + (userview_last ? 'All' : 'Own'));
  const [userView, setUserView] = React.useState(userview_last);

  let columns = [
    { title: 'Name', cellTransforms: [headerCol()], transforms: [sortable] },
    { title: 'AccessMode', transforms: [sortable] },
    { title: 'GID', transforms: [sortable] },
    { title: 'Experiments', transforms: [sortable] },
    { title: 'Members', transforms: [sortable] },
  ];

  const setMode = (pid, mode) => {
    fetch(conf.api + '/project/' + pid, {
      method: 'POST',
      credentials: 'include',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        accessMode: {
          value: mode,
        },
      }),
    })
      .then((resp) => {
        console.log('resp', resp);
        if (resp.ok) {
          addAlert('Project ' + pid + ' access mode set to ' + mode, 'success');
          setReload(!reload);
        }
        if (resp.status !== 200) {
          addAlert('Error: ' + resp.statusText, 'danger');
        }
        return;
      })
      .catch((error) => {
        console.log(error);
        addAlert(error.message, 'danger');
      });
  };

  let actions = [
    {
      title: 'Delete',
      onClick: (event, rowId, rowData) => {
        const projname = rowData.name.props.text;
        fetch(conf.api + '/project/' + projname, {
          method: 'DELETE',
          credentials: 'include',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            user: username,
            name: projname,
          }),
        })
          .then((resp) => {
            if (resp.ok) {
              addAlert('Project ' + projname + ' Deleted', 'success');
              setReload(!reload);
            }
            return;
          })
          .catch((error) => {
            console.log(error);
            addAlert(error.message, 'danger');
          });
      },
    },
    {
      isSeparator: true,
    },
    {
      title: 'Set Access Public',
      onClick: (event, rowId, rowData) => {
        const pid = rowData.name.props.text;
        setMode(pid, 'Public');
      },
    },
    {
      title: 'Set Access Protected',
      onClick: (event, rowId, rowData) => {
        const pid = rowData.name.props.text;
        setMode(pid, 'Protected');
      },
    },
    {
      title: 'Set Access Private',
      onClick: (event, rowId, rowData) => {
        const pid = rowData.name.props.text;
        setMode(pid, 'Private');
      },
    },
  ];

  let mapper = (json) => {
    return json.projects.map((x) => [
      {
        title: <Link to={'/project/' + x.name}>{x.name}</Link>,
        props: {
          component: 'th',
          text: x.name,
        },
      },
      x.accessMode,
      x.gid,
      x.experiments.length,
      Object.keys(x.members).length,
    ]);
  };

  const toggleNewProj = () => {
    setShowNew(!showNew);
  };

  const toggleView = () => {
    setViewLabel('View ' + (userView === false ? 'All' : 'Own'));
    localStorage.setItem('userview', userView ? '0' : '1');
    setUserView(!userView);
    setReload(!reload);
  };

  const addAlert = (t, v) => {
    setAlerts((prev) => [...prev, { title: t, variant: v }]);
  };

  const onProjNameChange = (val) => {
    const re = /^[a-z]([a-z0-9]*[a-z0-9])?$/;
    if (re.test(val) === false) {
      setNewProjName({ value: val, valid: 'error' });
      setInvalidName('The project name must start with a lowercase and only contain lowercase alphanumeric characters');
    } else {
      setInvalidName('');
      setNewProjName({ value: val, valid: val === '' ? 'error' : 'success' });
    }
  };

  const submitForm = () => {
    const members = {};
    members[username] = { State: 'Active', Role: 'Creator' };

    fetch(conf.api + '/project/' + newProjName.value, {
      method: 'PUT',
      credentials: 'include',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        user: username,
        project: {
          name: newProjName.value,
          description: newProjDesc,
          members: members,
        },
      }),
    })
      .then((resp) => {
        if (resp.ok) {
          addAlert('New Project ' + newProjName.value + ' Created', 'success');
          setReload(!reload);
          setNewProjDesc('');
          setNewProjName('');
          toggleNewProj();
          return;
        } else {
          return resp.text().then((text) => {
            throw new Error(text);
          });
        }
      })
      .catch((error) => {
        console.log(error);
        addAlert(error.message, 'danger');
      });
  };

  const newModal = (
    <Modal isOpen={showNew} onClose={toggleNewProj} variant={ModalVariant.medium} aria-label="New Project">
      <React.Fragment>
        <Title headingLevel="h1" size="2xl">
          New Project
        </Title>
        <Form>
          <FormGroup
            label="Name"
            fieldId="name"
            validated={newProjName.valid}
            isRequired={true}
            helperText={<FormHelperText isHidden={newProjName.valid !== 'success'} />}
            helperTextInvalid={invalidName}
          >
            <TextInput
              isRequired
              type="text"
              id="name"
              value={newProjName.value}
              onChange={(e) => onProjNameChange(e)}
            />
          </FormGroup>
          <FormGroup label="Description">
            <TextInput type="text" id="name" value={newProjDesc} onChange={(e) => setNewProjDesc(e)} />
          </FormGroup>
          <ActionGroup>
            <Button variant="control" onClick={submitForm}>
              Submit
            </Button>
          </ActionGroup>
        </Form>
      </React.Fragment>
    </Modal>
  );

  const crumbs = (
    <PageSection>
      <Breadcrumb>
        <BreadcrumbItem>Projects</BreadcrumbItem>
      </Breadcrumb>
    </PageSection>
  );

  const header = (
    <PageSection>
      <Split>
        <SplitItem>
          <Title headingLevel="h1" size="lg">
            Projects
          </Title>
        </SplitItem>
        <SplitItem isFilled />
        <SplitItem>
          <Button variant="control" aria-label={viewLabel} onClick={toggleView}>
            {viewLabel}
          </Button>
          <Button variant="control" aria-label="New Project" onClick={toggleNewProj}>
            New Project
          </Button>
        </SplitItem>
      </Split>
    </PageSection>
  );

  const notifications = (
    <AlertGroup isToast>
      {alerts.map((a, i) => (
        <Alert variant={AlertVariant[a.var]} title={a.title} timeout={3000} key={i} />
      ))}
    </AlertGroup>
  );

  let url = conf.api + '/project';
  if (userView === false) {
    url += '?filter=ByAll';
  }

  return (
    <React.Fragment>
      {alerts.length !== 0 && notifications}
      {newModal}
      {crumbs}
      {header}
      <PageSection>
        <ActionList kind="Project" columns={columns} url={url} actions={actions} mapper={mapper} reload={reload} />
      </PageSection>
    </React.Fragment>
  );
};

export { Projects };
