import * as React from 'react';
import { useParams } from 'react-router-dom';
import { useFetch } from 'use-http';
import { GeneralSettingsContext } from '@app/Settings/General/GeneralSettings';
import { ModelEditor } from '@app/ModelEditor/ModelEditor';
import { TopologyView } from '@app/Revision/TopologyView';
import {
  Alert,
  Spinner,
  Bullseye,
  PageSection,
  Card,
  CardBody,
  Split,
  SplitItem,
  Stack,
  StackItem,
  Tabs,
  Tab,
  TabTitleText,
  Breadcrumb,
  BreadcrumbItem,
  Button,
  Title,
  Modal,
  ModalVariant,
  Form,
  FormGroup,
  TextInput,
  ActionGroup,
  AlertGroup,
  AlertVariant,
} from '@patternfly/react-core';

const Revision: React.FunctionComponent = () => {
  const { pid, eid, rev } = useParams();
  const conf = React.useContext(GeneralSettingsContext);
  const [activeTab, setActiveTab] = React.useState(0);

  const [showRealRev, setShowRealRev] = React.useState(false);
  const [realName, setRealName] = React.useState('');
  const [alerts, setAlerts] = React.useState([]);

  const options = { credentials: 'include' };
  const { loading, error, data } = useFetch(
    conf.api + '/project/' + pid + '/experiment/' + eid + '/revision/' + rev + '?encoding=DOT',
    options,
    []
  );

  const tabClick = (event, index) => {
    setActiveTab(index);
  };

  const crumbs = (
    <PageSection>
      <Breadcrumb>
        <BreadcrumbItem to="/project">Projects</BreadcrumbItem>
        <BreadcrumbItem to={'/project/' + pid}>{pid}</BreadcrumbItem>
        <BreadcrumbItem to="/experiment">Experiments</BreadcrumbItem>
        <BreadcrumbItem to={'/project/' + pid + '/experiment/' + eid}>{eid}</BreadcrumbItem>
        <BreadcrumbItem>Revisions</BreadcrumbItem>
        <BreadcrumbItem>{rev}</BreadcrumbItem>
      </Breadcrumb>
    </PageSection>
  );

  const header = (
    <PageSection>
      <Split>
        <SplitItem>
          <Title headingLevel="h1" size="lg">
            Revision: {pid}/{eid}/{rev}
          </Title>
        </SplitItem>
        <SplitItem isFilled />
        <SplitItem>
          <Button variant="control" aria-label="New Project" onClick={() => setShowRealRev(!showRealRev)}>
            Realize Revision
          </Button>
        </SplitItem>
      </Split>
    </PageSection>
  );

  const addAlert = (t, v) => {
    setAlerts((prev) => [...prev, { title: t, variant: v }]);
  };

  const submitRealize = () => {
    fetch(conf.api + '/realize/realizations/' + pid + '/' + eid + '/' + realName, {
      method: 'PUT',
      credentials: 'include',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        project: pid,
        experiment: eid,
        realization: realName,
        revision: rev,
      }),
    })
      .then((resp) => {
        if (resp.ok) {
          addAlert('New Realization ' + realName + ' Created', 'success');
          setRealName('');
          setShowRealRev(!showRealRev);
          return;
        } else {
          return resp.text().then((text) => {
            throw new Error(text);
          });
        }
      })
      .catch((error) => {
        addAlert(error.message, 'danger');
        setShowRealRev(!showRealRev);
      });
  };

  const realizeModal = (
    <Modal
      isOpen={showRealRev}
      onClose={() => setShowRealRev(!showRealRev)}
      variant={ModalVariant.medium}
      aria-label="Realize Revision"
    >
      <React.Fragment>
        <Title headingLevel="h1" size="2xl">
          Realize Revision
        </Title>
        <Form>
          <FormGroup label="Project">
            <TextInput isRequired type="text" id="project" value={pid} isReadOnly={true} />
          </FormGroup>
          <FormGroup label="Experiment">
            <TextInput type="text" id="experment" value={eid} isReadOnly={true} />
          </FormGroup>
          <FormGroup label="Realization Name">
            <TextInput type="text" id="realization" value={realName} onChange={(e) => setRealName(e)} />
          </FormGroup>
          <ActionGroup>
            <Button variant="control" onClick={submitRealize}>
              Submit
            </Button>
          </ActionGroup>
        </Form>
      </React.Fragment>
    </Modal>
  );

  const notifications = (
    <AlertGroup isToast>
      {alerts.map((a, i) => (
        <Alert variant={AlertVariant[a.var]} title={a.title} timeout={3000} key={i} />
      ))}
    </AlertGroup>
  );

  return (
    <React.Fragment>
      {alerts.length !== 0 && notifications}
      {realizeModal}
      {crumbs}
      {header}
      <PageSection>
        <Card>
          <CardBody>
            {error && !data && (
              <Alert variant="danger" title="Error">
                Error loading
              </Alert>
            )}
            {error && data && data.hasOwnProperty('message') && (
              <Alert variant="danger" title="Response Error">
                <pre>{JSON.stringify(data, null, 2)}</pre>
              </Alert>
            )}
            {loading && (
              <Bullseye>
                <Spinner size="sm" />
              </Bullseye>
            )}
            {data && data.hasOwnProperty('model') && (
              <div>
                <Tabs activeKey={activeTab} onSelect={tabClick}>
                  <Tab eventKey={0} title={<TabTitleText>Topology</TabTitleText>}>
                    <TopologyView model={data.model.model} />
                  </Tab>
                  <Tab eventKey={1} title={<TabTitleText>Model</TabTitleText>}>
                    <ModelEditor model={data.ModelFile} readonly={true} />
                  </Tab>
                  <Tab eventKey={2} title={<TabTitleText>XIR</TabTitleText>}>
                    <pre>{JSON.stringify(data.model, null, 2)}</pre>
                  </Tab>
                  <Tab eventKey={3} title={<TabTitleText>Graphviz</TabTitleText>}>
                    <pre>{data.encoding}</pre>
                  </Tab>
                </Tabs>
              </div>
            )}
          </CardBody>
        </Card>
      </PageSection>
    </React.Fragment>
  );
};

export { Revision };
