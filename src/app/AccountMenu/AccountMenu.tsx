import * as React from 'react';
import { UserContext } from '@app/User/User';
import { GeneralSettingsContext } from '@app/Settings/General/GeneralSettings';
import { Avatar, Dropdown, DropdownToggle, DropdownItem } from '@patternfly/react-core';
import CaretDownIcon from '@patternfly/react-icons/dist/esm/icons/caret-down-icon';
import UserIcon from '@patternfly/react-icons/dist/esm/icons/user-icon';

const AccountMenu: React.FunctionComponent = () => {
  const conf = React.useContext(GeneralSettingsContext);
  const userTraits = React.useContext(UserContext);

  const [isOpen, setIsOpen] = React.useState(false);

  const logouthref = conf.auth + '/.ory/kratos/self-service/browser/flows/logout?return_to=' + window.location.href;

  const dropdownItems = [
    <DropdownItem key="settings" component={<a href={conf.auth + '/.login/web/settings'}>Identity Settings</a>} />,
    <DropdownItem key="logout" component={<a href={logouthref}>Logout</a>} />,
  ];

  return (
    <React.Fragment>
      <Dropdown
        onSelect={() => setIsOpen(!isOpen)}
        isOpen={isOpen}
        isPlain
        toggle={
          <DropdownToggle
            id="user-settings-dropdown"
            onToggle={(next) => setIsOpen(next)}
            toggleIndicator={CaretDownIcon}
            icon={<UserIcon />}
          >
            {userTraits.username}
          </DropdownToggle>
        }
        dropdownItems={dropdownItems}
      />
    </React.Fragment>
  );
};

export { AccountMenu };
